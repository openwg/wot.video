// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Andrey Andruschyshyn
// Copyright (c) 2022 Mikhail Paulyshka

#pragma once

// stdlib
#include <atomic>
#include <string>

// xfw.hooks
#include "xfw_native_hooks_d3d.h"

// wot.video
#include "wot_video_d3d.h"


namespace WoT::Video {

    class CaptureScreenshot {
      public:
        CaptureScreenshot(XFW::Native::Hooks::HookmanagerD3D& hookmanager_d3d);
        ~CaptureScreenshot();

        bool Init(std::shared_ptr<D3D> d3d);
        bool Deinit();
        bool Capture(const std::wstring &filename);

      private:
        void process();

      private:
        void onDxgiSwapchainPresent(IDXGISwapChain *swapChain, UINT, UINT);

      private:
        std::wstring _filename{};
        std::atomic_flag _active{};
        std::shared_ptr<D3D> _d3d{};
        XFW::Native::Hooks::HookmanagerD3D &_hooks_d3d;
        bool _inited = false;
    };
} // namespace WoT::Video
