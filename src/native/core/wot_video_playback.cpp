// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Andrey Andruschyshyn
// Copyright (c) 2022,2025 Mikhail Paulyshka

// stdlib
#include <algorithm>
#include <chrono>
#include <functional>

// windows
#include <d3dcompiler.h>

// wot.video
#include "wot_video_d3d_shaders.h"
#include "wot_video_playback.h"
#include "wot_video_playback_resource_image.h"
#include "wot_video_playback_resource_video.h"



using namespace std::placeholders;

namespace WoT::Video {


    //
    // Init
    //

    PlaybackVideo::PlaybackVideo() {}

    PlaybackVideo::~PlaybackVideo() { Deinit(); }

    bool PlaybackVideo::Init(std::shared_ptr<CoreAudio> coreaudio, std::shared_ptr<D3D> d3d, std::shared_ptr<VideoPlatform> mf_platform) {
        m_audio_core = std::move(coreaudio);
        m_render_d3d = std::move(d3d);
        m_mf_platform = std::move(mf_platform);

        if (!renderInit()) {
            Deinit();
            return false;
        }

        _inited = true;
        return true;
    }

    bool PlaybackVideo::Deinit() {
        _inited = false;
        renderDeinit();
        m_render_d3d.reset();
        m_mf_platform.reset();
        m_audio_core.reset();
        return true;
    }



    //
    // Resource
    //

    int PlaybackVideo::ResourceAddImage(const std::wstring &filename) {
        std::scoped_lock lock(m_resource_mutex);

        auto resource = std::make_unique<PlaybackResourceImage>(m_render_d3d);

        if (!resource->Init(filename)) {
            return false;
        }

        m_resource_cnt++;
        int cookie = m_resource_cnt;
        m_resource_map[cookie] = std::move(resource);
        return cookie;
    }

    int PlaybackVideo::ResourceAddVideo(const std::wstring &filename) {
        std::scoped_lock lock(m_resource_mutex);

        auto resource = std::make_unique<PlaybackResourceVideo>(m_audio_core, m_render_d3d);

        if (!resource->Init(filename, m_mf_platform->MFGetDeviceManager())) {
            return false;
        }

        m_resource_cnt++;
        int cookie = m_resource_cnt;
        m_resource_map[cookie] = std::move(resource);
        return cookie;
    }

    int PlaybackVideo::ResourceCount() const { return m_resource_map.size(); }

    bool PlaybackVideo::ResourcePositionSet(int cookie, int x, int y) {
        std::scoped_lock lock(m_resource_mutex);
        if (!m_resource_map.contains(cookie)) {
            return false;
        }

        return m_resource_map[cookie]->PositionSet(x, y);
    }

    bool PlaybackVideo::ResourceMaskSet(int cookie, const std::wstring &filename) {
        std::scoped_lock lock(m_resource_mutex);
        if (!m_resource_map.contains(cookie)) {
            return false;
        }

        return m_resource_map[cookie]->MaskSet(filename);
    }

    bool PlaybackVideo::ResourceSizeSet(int cookie, int width, int height) {
        std::scoped_lock lock(m_resource_mutex);
        if (!m_resource_map.contains(cookie)) {
            return false;
        }

        return m_resource_map[cookie]->SizeSet(width, height);
    }

    bool PlaybackVideo::ResourceVisiblitySet(int cookie, bool val) {
        std::scoped_lock lock(m_resource_mutex);
        if (!m_resource_map.contains(cookie)) {
            return false;
        }

        return m_resource_map[cookie]->VisibilitySet(val);
    }

    bool PlaybackVideo::ResourceVisiblityOnFinishSet(int cookie, bool val) {
        std::scoped_lock lock(m_resource_mutex);
        if (!m_resource_map.contains(cookie)) {
            return false;
        }

        return m_resource_map[cookie]->VisibilityOnFinishSet(val);
    }



    //
    // Render
    //

    bool PlaybackVideo::renderInit() {
        if (!renderInitHandlers()) {
            return false;
        }

        if (!renderVsCompile()) {
            return false;
        }

        if (!renderPsCompile()) {
            return false;
        }
        if (!renderPsCreateSampler()) {
            return false;
        }

        if (!renderIaCreateLayout()) {
            return false;
        }
        if (!renderIaCreateQuad()) {
            return false;
        }

        if (!renderRsCreateState()) {
            return false;
        }

        if (!renderOmCreateBlendstate()) {
            return false;
        }
        if (!renderOmCreateRTV()) {
            return false;
        }

        return true;
    }

    bool PlaybackVideo::renderDeinit() { return renderDeinitHandlers(); }

    bool PlaybackVideo::renderInitHandlers() {
        m_render_d3d->HandlerPresentRegister("wot_video_playback", [this](){renderDraw();});
        m_render_d3d->HandlerResizeBeforeRegister("wot_video_playback", [this](){renderOmClearRTV();});
        m_render_d3d->HandlerSwapchainRegister("wot_video_playback", [this](){renderOmCreateRTV();});
        return true;
    }

    bool PlaybackVideo::renderDeinitHandlers() {
        m_render_d3d->HandlerPresentUnregister("wot_video_playback");
        m_render_d3d->HandlerResizeBeforeUnregister("wot_video_playback");
        m_render_d3d->HandlerSwapchainUnregister("wot_video_playback");
        return true;
    }

    void PlaybackVideo::renderDraw() {
        if (!ResourceCount()) {
            return;
        }

        auto* ctx = m_render_d3d->DeviceGetContext();

        // Common/IA
        UINT stride = m_render_is_quad_vertexsize;
        UINT offset = 0;
        ctx->IASetInputLayout(m_render_ia_layout.Get());
        ctx->IASetVertexBuffers(0, 1, m_render_ia_quad.GetAddressOf(), &stride, &offset);
        ctx->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);

        // Common/VS
        ctx->VSSetShader(m_render_vs_shader.Get(), nullptr, 0);

        // Common/RS
        ctx->RSSetState(m_render_rs_state.Get());

        // Common/PS
        ctx->PSSetSamplers(0, 1, m_render_ps_sampler.GetAddressOf());

        // Comon/OM
        ctx->OMSetRenderTargets(1, m_render_om_rtv.GetAddressOf(), nullptr);
        ctx->OMSetBlendState(m_render_om_blendstate.Get(), nullptr, 0xFFFFFFFF);

        {
            std::scoped_lock lock(m_resource_mutex);
            for (auto& resource: m_resource_map) {
                resource.second->RenderProcess();

                if (!resource.second->VisibilityGet()) {
                    continue;
                }

                // Set PS
                auto texture_srv = resource.second->RsGetSRV();
                if (texture_srv.size() == 3) {
                    ctx->PSSetShader(m_render_ps_nv12_mask_shader.Get(), nullptr, 0);
                }
                else if (texture_srv.size() == 2) {
                    ctx->PSSetShader(m_render_ps_nv12_shader.Get(), nullptr, 0);
                }
                else if (texture_srv.size() == 1) {
                    ctx->PSSetShader(m_render_ps_rgb_shader.Get(), nullptr, 0);
                }
                else {
                    continue;
                }
                ctx->PSSetShaderResources(0, texture_srv.size(), texture_srv.data());

                // Set RS
                auto viewport = resource.second->RsGetViewport();
                ctx->RSSetViewports(1, &viewport);

                // Draw
                ctx->Draw(4, 0);
            }
        }
    }



    //
    // Render/IA
    //

    bool PlaybackVideo::renderIaCreateLayout() {
        D3D11_INPUT_ELEMENT_DESC layout[] = {
            {"POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
            {"TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 8, D3D11_INPUT_PER_VERTEX_DATA, 0},
        };

        if (FAILED(m_render_d3d->DeviceGet()->CreateInputLayout(layout, 2, m_render_vs_blob->GetBufferPointer(), m_render_vs_blob->GetBufferSize(),
                                                         m_render_ia_layout.GetAddressOf()))) {
            return false;
        }

        return true;
    }

    bool PlaybackVideo::renderIaCreateQuad() {
        struct Vertex {
            float position[2];
            float texcoord[2];
        };
        m_render_is_quad_vertexsize = sizeof(Vertex);

        Vertex vertices[] = {
            {{-1, -1}, {0, 0}}, // Bottom-left
            {{1, -1}, {1, 0}},  // Bottom-right
            {{-1, 1}, {0, 1}},  // Top-left
            {{1, 1}, {1, 1}},   // Top-right
        };

        D3D11_BUFFER_DESC bufferDesc = {};
        bufferDesc.ByteWidth = sizeof(Vertex) * 4;
        bufferDesc.Usage = D3D11_USAGE_DEFAULT;
        bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

        D3D11_SUBRESOURCE_DATA initData = {};
        initData.pSysMem = vertices;

        if (FAILED(m_render_d3d->DeviceGet()->CreateBuffer(&bufferDesc, &initData, m_render_ia_quad.GetAddressOf()))) {
            return false;
        }

        return true;
    }



    //
    // Render/VS
    //

    bool PlaybackVideo::renderVsCompile() {
        // Compile vertex shader
        if (FAILED(D3DCompile(D3DShaders::VS_Quad.data(), D3DShaders::VS_Quad.size(), nullptr, nullptr, nullptr, "main", "vs_5_0", 0, 0, m_render_vs_blob.GetAddressOf(), nullptr))) {
            return false;
        }
        if (FAILED(m_render_d3d->DeviceGet()->CreateVertexShader(m_render_vs_blob->GetBufferPointer(), m_render_vs_blob->GetBufferSize(), nullptr,
                                                          m_render_vs_shader.GetAddressOf()))) {
            return false;
                                                          }

        return true;
    }



    //
    // Render/RS
    //

    bool PlaybackVideo::renderRsCreateState() {
        D3D11_RASTERIZER_DESC rasterDesc = {};
        rasterDesc.FillMode = D3D11_FILL_SOLID;
        rasterDesc.CullMode = D3D11_CULL_NONE;
        rasterDesc.FrontCounterClockwise = FALSE;
        rasterDesc.DepthClipEnable = TRUE;

        if (FAILED(m_render_d3d->DeviceGet()->CreateRasterizerState(&rasterDesc, m_render_rs_state.GetAddressOf()))) {
            return false;
        }

        return true;
    }



    //
    // Render/PS
    //

    bool PlaybackVideo::renderPsCompile() {
        if (FAILED(D3DCompile(D3DShaders::PS_Sampler_RGB.data(), D3DShaders::PS_Sampler_RGB.size(), nullptr, nullptr, nullptr, "main", "ps_5_0", 0, 0, m_render_ps_rgb_blob.GetAddressOf(), nullptr))) {
            return false;
        }

        if (FAILED(m_render_d3d->DeviceGet()->CreatePixelShader(m_render_ps_rgb_blob->GetBufferPointer(), m_render_ps_rgb_blob->GetBufferSize(), nullptr,
                                                         m_render_ps_rgb_shader.GetAddressOf()))) {
            return false;
                                                         }

        if (FAILED(D3DCompile(D3DShaders::PS_Sampler_NV12.data(), D3DShaders::PS_Sampler_NV12.size(), nullptr, nullptr, nullptr, "main", "ps_5_0", 0, 0, m_render_ps_nv12_blob.GetAddressOf(), nullptr))) {
            return false;
        }

        if (FAILED(m_render_d3d->DeviceGet()->CreatePixelShader(m_render_ps_nv12_blob->GetBufferPointer(), m_render_ps_nv12_blob->GetBufferSize(), nullptr,
                                                         m_render_ps_nv12_shader.GetAddressOf()))) {
            return false;
                                                         }

        if (FAILED(D3DCompile(D3DShaders::PS_Sampler_NV12_Mask.data(), D3DShaders::PS_Sampler_NV12_Mask.size(), nullptr, nullptr, nullptr, "main", "ps_5_0", 0, 0, m_render_ps_nv12_mask_blob.GetAddressOf(), nullptr))) {
            return false;
        }

        if (FAILED(m_render_d3d->DeviceGet()->CreatePixelShader(m_render_ps_nv12_mask_blob->GetBufferPointer(), m_render_ps_nv12_mask_blob->GetBufferSize(), nullptr,
                                                         m_render_ps_nv12_mask_shader.GetAddressOf()))) {
            return false;
                                                         }

        return true;
    }

    bool PlaybackVideo::renderPsCreateSampler() {
        D3D11_SAMPLER_DESC samplerDesc = {};
        samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR; // Linear filtering
        samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;   // Clamp addressing
        samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;   // Clamp addressing
        samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;   // Clamp addressing
        samplerDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;  // No comparison
        samplerDesc.MinLOD = 0;                               // Minimum LOD
        samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;               // Maximum LOD

        if (FAILED(m_render_d3d->DeviceGet()->CreateSamplerState(&samplerDesc, m_render_ps_sampler.GetAddressOf()))) {
            return false;
        }
        return true;
    }



    //
    // Render/OM
    //

    bool PlaybackVideo::renderOmCreateBlendstate() {
        D3D11_BLEND_DESC blendDesc = {};
        blendDesc.RenderTarget[0].BlendEnable = TRUE;
        blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
        blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
        blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;

        blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
        blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
        blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;

        blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL; // Allow writes to RGBA

        if (FAILED(m_render_d3d->DeviceGet()->CreateBlendState(&blendDesc, m_render_om_blendstate.GetAddressOf()))) {
            return false;
        }

        return true;
    }

    bool PlaybackVideo::renderOmCreateRTV() {
        auto backbuffer = m_render_d3d->SwapchainGetBackbuffer();
        if (!backbuffer) {
            return false;
        }

        if (FAILED(m_render_d3d->DeviceGet()->CreateRenderTargetView(backbuffer.Get(), nullptr, m_render_om_rtv.ReleaseAndGetAddressOf()))) {
            return false;
        }

        return true;
    }

    bool PlaybackVideo::renderOmClearRTV() {
        m_render_om_rtv.Reset();
        return true;
    }

} // namespace WoT::Video
