#pragma once

//
// Includes
//

// stdlib
#include <cstdint>

// windows
#include <wrl.h>
#include <mfobjects.h>



//
// Class
//

namespace WoT::Video {

    struct VideoSample {
        Microsoft::WRL::ComPtr<IMFSample> sample{};
        int64_t timestamp{};
        int64_t timestamp_end{};
        int64_t duration{};
        uint32_t stream_idx{};
        uint32_t flags{};
    };

}
