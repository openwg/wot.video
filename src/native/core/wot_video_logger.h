// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Mikhail Paulyshka

#pragma once

// stdlib
#include <sstream>
#include <string>

// Windows
#include <windows.h>



namespace WoT::Video {

    namespace wot_video_logger {
        template <class... Args> static void log(Args... args) {
            std::stringstream ss;
            (ss << ... << args) << "\n";

            auto str = ss.str();
            OutputDebugStringA(str.c_str());
        }
    } // namespace wot_video_logger
} // namespace WoT::Video
