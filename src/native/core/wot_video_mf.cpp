// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Andrey Andruschyshyn
// Copyright (c) 2022 Mikhail Paulyshka

// stdlib
#include <filesystem>

// windows
#include <windows.h>
#include <mfapi.h>
#include <propvarutil.h>

// wot.video
#include "wot_video_mf.h"



namespace WoT::Video {

    namespace MF {
        //
        // Media Types
        //
        GUID _mediatype_audio_in_format = MFAudioFormat_PCM;
        GUID _mediatype_audio_out_format = MFAudioFormat_AAC;
        GUID _mediatype_video_in_format = MFVideoFormat_ARGB32;
        GUID _mediatype_video_out_format = MFVideoFormat_H264;

        uint32_t _mediatype_audio_in_bitness = 16U;
        uint32_t _mediatype_audio_out_bitness = 16U;


        bool MediaTypeEquals(Microsoft::WRL::ComPtr<IMFMediaType> &type_1, Microsoft::WRL::ComPtr<IMFMediaType> &type_2) {
            bool result = false;

            if (type_1 && type_2) {
                BOOL compare_result{};
                if (SUCCEEDED(type_1->Compare(type_2.Get(), MF_ATTRIBUTES_MATCH_INTERSECTION, &compare_result))) {
                    result = compare_result;
                }
            }

            return result;
        }

        Microsoft::WRL::ComPtr<IMFMediaType> MediaTypeGetAudioIn(uint32_t frequency, uint32_t channels) {
            Microsoft::WRL::ComPtr<IMFMediaType> result;

            if (!frequency || !channels) {
                return {};
            }

            if (FAILED(MFCreateMediaType(result.GetAddressOf()))) {
                return {};
            }

            if (FAILED(result->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Audio))) {
                return {};
            }

            if (FAILED(result->SetGUID(MF_MT_SUBTYPE, _mediatype_audio_in_format))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_AUDIO_BITS_PER_SAMPLE, _mediatype_audio_in_bitness))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_AUDIO_SAMPLES_PER_SECOND, frequency))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_AUDIO_NUM_CHANNELS, channels))) {
                return {};
            }

            return result;
        }

        Microsoft::WRL::ComPtr<IMFMediaType> MediaTypeGetAudioIn(Microsoft::WRL::ComPtr<IMFMediaType> type_in) {
            Microsoft::WRL::ComPtr<IMFMediaType> result{};

            UINT32 prop_channels{};
            UINT32 prop_samplerate{};
            UINT32 prop_bps;

            // read native type
            if (FAILED(type_in->GetUINT32(MF_MT_AUDIO_NUM_CHANNELS, &prop_channels))) {
                return {};
            }
            if (FAILED(type_in->GetUINT32(MF_MT_AUDIO_BITS_PER_SAMPLE, &prop_bps))) {
                return {};
            }
            if (FAILED(type_in->GetUINT32(MF_MT_AUDIO_SAMPLES_PER_SECOND, &prop_samplerate))) {
                return {};
            }

            // create target type
            if (FAILED(MFCreateMediaType(result.GetAddressOf()))) {
                return {};
            }

            if (FAILED(result->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Audio))) {
                return {};
            }

            if (FAILED(result->SetGUID(MF_MT_SUBTYPE, _mediatype_audio_in_format))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_AUDIO_BITS_PER_SAMPLE, prop_bps))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_AUDIO_SAMPLES_PER_SECOND, prop_samplerate))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_AUDIO_NUM_CHANNELS, prop_channels))) {
                return {};
            }

            return result;
        }

        Microsoft::WRL::ComPtr<IMFMediaType> MediaTypeGetAudioOut(uint32_t frequency, uint32_t channels, uint32_t bitrate) {
            Microsoft::WRL::ComPtr<IMFMediaType> result;

            if (!frequency || !channels) {
                return {};
            }

            if (FAILED(MFCreateMediaType(result.GetAddressOf()))) {
                return {};
            }

            if (FAILED(result->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Audio))) {
                return {};
            }

            if (FAILED(result->SetGUID(MF_MT_SUBTYPE, _mediatype_audio_out_format))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_AUDIO_BITS_PER_SAMPLE, _mediatype_audio_out_bitness))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_AUDIO_SAMPLES_PER_SECOND, frequency))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_AUDIO_NUM_CHANNELS, channels))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_AUDIO_AVG_BYTES_PER_SECOND, bitrate / 8))) {
                return {};
            }

            return result;
        }

        Microsoft::WRL::ComPtr<IMFMediaType> MediaTypeGetAudioOut(Microsoft::WRL::ComPtr<IMFMediaType> type_in, uint32_t bitrate) {
            Microsoft::WRL::ComPtr<IMFMediaType> result{};

            UINT32 prop_channels{};
            UINT32 prop_samplerate{};
            UINT32 prop_bps;

            // read native type
            if (FAILED(type_in->GetUINT32(MF_MT_AUDIO_NUM_CHANNELS, &prop_channels))) {
                return {};
            }
            if (FAILED(type_in->GetUINT32(MF_MT_AUDIO_BITS_PER_SAMPLE, &prop_bps))) {
                return {};
            }
            if (FAILED(type_in->GetUINT32(MF_MT_AUDIO_SAMPLES_PER_SECOND, &prop_samplerate))) {
                return {};
            }

            // create target type
            if (FAILED(MFCreateMediaType(result.GetAddressOf()))) {
                return {};
            }

            if (FAILED(result->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Audio))) {
                return {};
            }

            if (FAILED(result->SetGUID(MF_MT_SUBTYPE, _mediatype_audio_out_format))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_AUDIO_BITS_PER_SAMPLE, prop_bps))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_AUDIO_SAMPLES_PER_SECOND, prop_samplerate))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_AUDIO_NUM_CHANNELS, prop_channels))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_AUDIO_AVG_BYTES_PER_SECOND, bitrate / 8))) {
                return {};
            }

            return result;
        }

        Microsoft::WRL::ComPtr<IMFMediaType> MediaTypeGetVideoIn(uint32_t width, uint32_t height) {
            Microsoft::WRL::ComPtr<IMFMediaType> result;

            if (!width || !height) {
                return {};
            }

            if (FAILED(MFCreateMediaType(result.GetAddressOf()))) {
                return {};
            }

            if (FAILED(result->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Video))) {
                return {};
            }

            if (FAILED(result->SetGUID(MF_MT_SUBTYPE, _mediatype_video_in_format))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_INTERLACE_MODE, MFVideoInterlace_Progressive))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_ALL_SAMPLES_INDEPENDENT, TRUE))) {
                return {};
            }

            if (FAILED(MFSetAttributeSize(result.Get(), MF_MT_FRAME_SIZE, width, height))) {
                return {};
            }

            if (FAILED(MFSetAttributeRatio(result.Get(), MF_MT_PIXEL_ASPECT_RATIO, 1U, 1U))) {
                return {};
            }

            return result;
        }

        Microsoft::WRL::ComPtr<IMFMediaType> MediaTypeGetVideoOut(uint32_t width, uint32_t height, uint32_t fps, uint32_t bitrate) {
            Microsoft::WRL::ComPtr<IMFMediaType> result;

            if (FAILED(MFCreateMediaType(result.GetAddressOf()))) {
                return {};
            }

            if (FAILED(result->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Video))) {
                return {};
            }

            if (FAILED(result->SetGUID(MF_MT_SUBTYPE, _mediatype_video_out_format))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_INTERLACE_MODE, MFVideoInterlace_Progressive))) {
                return {};
            }

            if (FAILED(result->SetUINT32(MF_MT_AVG_BITRATE, bitrate))) {
                return {};
            }

            if (FAILED(MFSetAttributeSize(result.Get(), MF_MT_FRAME_SIZE, width, height))) {
                return {};
            }

            if (FAILED(MFSetAttributeRatio(result.Get(), MF_MT_FRAME_RATE, fps, 1U))) {
                return {};
            }

            if (FAILED(MFSetAttributeRatio(result.Get(), MF_MT_PIXEL_ASPECT_RATIO, 1U, 1U))) {
                return {};
            }

            return result;
        }

        Microsoft::WRL::ComPtr<IMFMediaType> MediaTypeGetVideoTarget(Microsoft::WRL::ComPtr<IMFMediaType> type_in) {
            Microsoft::WRL::ComPtr<IMFMediaType> type_out{};

            GUID prop_subtype{};
            UINT32 prop_width{};
            UINT32 prop_height{};
            UINT32 prop_ratio_num{};
            UINT32 prop_ration_denom{};
            UINT32 prop_aspect_x{};
            UINT32 prop_aspect_y{};
            UINT32 prop_interlacing{};
            UINT32 prop_bitrate{};

            // reading native
            if (FAILED(type_in->GetGUID(MF_MT_SUBTYPE, &prop_subtype))) {
                return {};
            }
            if (FAILED(type_in->GetUINT32(MF_MT_INTERLACE_MODE, &prop_interlacing)) || (MFVideoInterlace_Unknown == prop_interlacing)) {
                prop_interlacing = MFVideoInterlace_Progressive;
            }
            if (FAILED(MFGetAttributeSize(type_in.Get(), MF_MT_FRAME_SIZE, &prop_width, &prop_height))) {
                return {};
            }
            if (FAILED(MFGetAttributeRatio(type_in.Get(), MF_MT_FRAME_RATE, &prop_ratio_num, &prop_ration_denom))) {
                return {};
            }
            if (FAILED(MFGetAttributeRatio(type_in.Get(), MF_MT_PIXEL_ASPECT_RATIO, &prop_aspect_x, &prop_aspect_y))) {
                prop_aspect_x = 1;
                prop_aspect_y = 1;
            }
            prop_bitrate = MFGetAttributeUINT32(type_in.Get(), MF_MT_AVG_BITRATE, 500000);


            // creating taget
            if (FAILED(MFCreateMediaType(type_out.GetAddressOf()))) {
                return {};
            }
            if (FAILED(type_out->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Video))) {
                return {};
            }
            if (FAILED(type_out->SetGUID(MF_MT_SUBTYPE, prop_subtype))) {
                return {};
            }
            if (FAILED(MFSetAttributeSize(type_out.Get(), MF_MT_FRAME_SIZE, prop_width, prop_height))) {
                return {};
            }
            if (FAILED(MFSetAttributeSize(type_out.Get(), MF_MT_FRAME_RATE, prop_ratio_num, prop_ration_denom))) {
                return {};
            }
            if (FAILED(MFSetAttributeSize(type_out.Get(), MF_MT_PIXEL_ASPECT_RATIO, prop_aspect_x, prop_aspect_y))) {
                return {};
            }
            if (FAILED(type_out->SetUINT32(MF_MT_INTERLACE_MODE, prop_interlacing))) {
                return {};
            }
            if (FAILED(type_out->SetUINT32(MF_MT_AVG_BITRATE, prop_bitrate))) {
                return {};
            }

            return type_out;
        }



        //
        // IMFMediaBuffer
        //

        Microsoft::WRL::ComPtr<IMFMediaBuffer> BufferCreateMemory(size_t buf_len) {
            Microsoft::WRL::ComPtr<IMFMediaBuffer> result;

            if (FAILED(MFCreateMemoryBuffer(buf_len, result.GetAddressOf()))) {
                return {};
            }
            if (FAILED(result->SetCurrentLength(buf_len))) {
                return {};
            }

            return result;
        }

        Microsoft::WRL::ComPtr<IMFMediaBuffer> BufferCreateMemory(const void *data, size_t data_len) {
            auto result = BufferCreateMemory(data_len);
            if (!result) {
                return {};
            }

            uint8_t *result_content = nullptr;

            if (FAILED(result->Lock(&result_content, nullptr, nullptr))) {
                return {};
            }
            std::memcpy(result_content, data, data_len);
            if (FAILED(result->Unlock())) {
                return {};
            }

            return result;
        }

        Microsoft::WRL::ComPtr<IMFMediaBuffer> BufferCreateTexture(Microsoft::WRL::ComPtr<ID3D11Texture2D> &texture) {
            Microsoft::WRL::ComPtr<IMFMediaBuffer> result;

            Microsoft::WRL::ComPtr<IMF2DBuffer> buf_2d;
            DWORD buffer_2d_len = 0U;

            // MF -> Buffer
            if (FAILED(MFCreateDXGISurfaceBuffer(__uuidof(texture), texture.Get(), 0U, FALSE, result.GetAddressOf()))) {
                return {};
            }
            if (FAILED(result->QueryInterface(IID_PPV_ARGS(&buf_2d)))) {
                return {};
            }
            if (FAILED(buf_2d->GetContiguousLength(&buffer_2d_len))) {
                return {};
            }
            if (FAILED(result->SetCurrentLength(buffer_2d_len))) {
                return {};
            }

            return result;
        }



        //
        // IMFSample
        //

        Microsoft::WRL::ComPtr<IMFSample> SampleCreate(Microsoft::WRL::ComPtr<IMFMediaBuffer> &buffer, int64_t timestamp_ns,
                                                       int64_t duration_ns) {

            Microsoft::WRL::ComPtr<IMFSample> result;

            if (!buffer) {
                return {};
            }
            if (FAILED(MFCreateSample(result.GetAddressOf()))) {
                return {};
            }
            if (FAILED(result->AddBuffer(buffer.Get()))) {
                return {};
            }
            if (FAILED(result->SetSampleTime(timestamp_ns / 100U))) {
                return {};
            }

            if (duration_ns) {
                if (FAILED(result->SetSampleDuration(duration_ns / 100U))) {
                    return {};
                }
            }

            return result;
        }

        Microsoft::WRL::ComPtr<IMFSample> SampleCopy(Microsoft::WRL::ComPtr<IMFSample> &sample) {
            Microsoft::WRL::ComPtr<IMFMediaBuffer> buffer{};

            int64_t sample_duration = 0;
            int64_t sample_timestamp = 0;

            if (FAILED(sample->GetSampleDuration(&sample_duration))) {
                return {};
            }
            if (FAILED(sample->GetSampleTime(&sample_timestamp))) {
                return {};
            }
            if (FAILED(sample->ConvertToContiguousBuffer(buffer.GetAddressOf()))) {
                return {};
            }

            return SampleCreate(buffer, sample_timestamp * 100ULL, sample_duration * 100ULL);
        }

    } // namespace MF

} // namespace WoT::Video
