// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Mikhail Paulyshka

// windows
#include <mmdeviceapi.h>

// wrl
#include <wrl.h>

// wot.video
#include "wot_video_coreaudio.h"



namespace WoT::Video {
    //
    // Init
    //

    bool CoreAudio::Init(IMMDevice* pDevice) {
        Deinit();

        if (!pDevice) {
            return false;
        }

        Microsoft::WRL::ComPtr<IAudioClient> audio_client;
        if (FAILED(pDevice->Activate(__uuidof(IAudioClient), CLSCTX_ALL, nullptr, reinterpret_cast<void **>(audio_client.GetAddressOf())))) {
            return false;
        }

        WAVEFORMATEX *format{};
        if (FAILED(audio_client->GetMixFormat(&format))) {
            return false;
        }

        if (!waveFormatInit(format)) {
            return false;
        }

        _inited = true;
        m_device = pDevice;

        return true;
    }


    bool CoreAudio::Deinit() {
        bool result = false;

        if (IsInited()) {
            waveFormatDeinit();

            _inited = false;
            result = true;
        }
        m_device = nullptr;
        return result;
    }

    bool CoreAudio::IsInited() const { return _inited; }


    //
    // MMDevice
    //

    IMMDevice *CoreAudio::MMDeviceGet() {
        return m_device;
    }


    //
    // WaveFormat
    //

    uint32_t CoreAudio::WaveFormatChannels() const { return _waveFormatChannels; }

    uint32_t CoreAudio::WaveFormatBitsPerSample() const { return _waveFormatBits; }

    uint32_t CoreAudio::WaveFormatFrequency() const { return _waveFormatFrequency; }

    bool CoreAudio::WaveFormatIsFloat() const { return _waveFormat == WAVE_FORMAT_IEEE_FLOAT; }

    bool CoreAudio::waveFormatInit(const WAVEFORMATEX *pFormat) {
        bool result{false};

        waveFormatDeinit();
        if (pFormat) {
            _waveFormatChannels = pFormat->nChannels;
            _waveFormatBits = pFormat->wBitsPerSample;
            _waveFormatFrequency = pFormat->nSamplesPerSec;

            if (pFormat->wFormatTag == WAVE_FORMAT_EXTENSIBLE) {
                const auto *format = reinterpret_cast<const WAVEFORMATEXTENSIBLE *>(pFormat);
                if (format->SubFormat == MEDIASUBTYPE_IEEE_FLOAT) {
                    _waveFormat = WAVE_FORMAT_IEEE_FLOAT;
                }
            } else {
                _waveFormat = pFormat->wFormatTag;
            }
            result = true;
        }
        
        return result;
    }

    void CoreAudio::waveFormatDeinit() {
        _waveFormatChannels = 0U;
        _waveFormatBits = 0U;
        _waveFormat = 0U;
        _waveFormatFrequency = 0U;
    }


} // namespace WoT::Video
