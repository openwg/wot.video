#pragma once

//
// Includes
//

// WoT.Video
#include "wot_video_playback_resource.h"



//
// Implementation
//

namespace WoT::Video {
    PlaybackResource::PlaybackResource(std::shared_ptr<D3D> d3d) : m_render_d3d(d3d) {}

    //
    // Position
    //

    bool PlaybackResource::PositionSet(int x, int y) {
        m_position_x = x;
        m_position_y = y;
        return true;
    }



    //
    // RS
    //

    std::vector<ID3D11ShaderResourceView*> PlaybackResource::RsGetSRV() {
        std::vector<ID3D11ShaderResourceView*> result;
        for (auto& element : m_rs_srv) {
            result.push_back(element.Get());
        }
        return result;
    }

    D3D11_VIEWPORT PlaybackResource::RsGetViewport() {
        D3D11_VIEWPORT viewport{};
        viewport.TopLeftX = static_cast<float>(m_position_x);
        viewport.TopLeftY = static_cast<float>(m_position_y);
        viewport.MinDepth = 0.0f;
        viewport.MaxDepth = 1.0f;
        viewport.Width = static_cast<float>(m_size_width);
        viewport.Height = static_cast<float>(m_size_height);

        return viewport;
    }



    //
    // Size
    //

    std::pair<int, int> PlaybackResource::SizeGet() const {
        return {m_size_width, m_size_height};
    }

    bool PlaybackResource::SizeSet(int width, int height) {
        m_size_width = width;
        m_size_height = height;
        return true;
    }



    //
    // Visibility
    //

    bool PlaybackResource::VisibilityGet() const {
        return m_visibility;
    }

    bool PlaybackResource::VisibilitySet(bool val) {
        m_visibility = val;
        return true;
    }

    bool PlaybackResource::VisibilityOnFinishSet(bool val) {
        return false;
    }
} // namespace WoT::Video
