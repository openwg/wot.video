// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Andrey Andruschyshyn
// Copyright (c) 2022,2025 Mikhail Paulyshka

#pragma once

//
// Includes
//

// stdlib
#include <atomic>
#include <memory>
#include <mutex>
#include <string>

// Windows
#include <wrl.h>

// Direct3D
#include <d3d11.h>

// xfw.native.hooks
#include "xfw_native_hooks_d3d.h"

// wot.video
#include "wot_video_coreaudio.h"
#include "wot_video_d3d.h"
#include "wot_video_mf_platform.h"
#include "wot_video_playback_resource.h"



//
// Class
//

namespace WoT::Video {
    class PlaybackVideo {
        //
        // Init
        //
      public:
        PlaybackVideo();
        ~PlaybackVideo();
        bool Init(std::shared_ptr<CoreAudio> coreaudio, std::shared_ptr<D3D> d3d, std::shared_ptr<VideoPlatform> mf_platform);
        bool Deinit();

      private:
        bool _inited = false;

        //
        // Audio
        //
    private:
        std::shared_ptr<CoreAudio> m_audio_core;

        //
        // MF
        //
    private:
        std::shared_ptr<VideoPlatform> m_mf_platform;

        //
        // Resource
        //
    public:
        int ResourceAddImage(const std::wstring &filename);
        int ResourceAddVideo(const std::wstring &filename);
        [[nodiscard]] int ResourceCount() const;
        bool ResourcePositionSet(int cookie, int x, int y);
        bool ResourceMaskSet(int cookie, const std::wstring &filename);
        bool ResourceSizeSet(int cookie, int width, int height);
        bool ResourceVisiblitySet(int cookie, bool val);
        bool ResourceVisiblityOnFinishSet(int cookie, bool val);

    private:
        std::map<int, std::unique_ptr<PlaybackResource>> m_resource_map;
        int m_resource_cnt{};
        std::mutex m_resource_mutex;


        //
        // Render
        //
    private:
        bool renderInit();
        bool renderDeinit();
        bool renderInitHandlers();
        bool renderDeinitHandlers();
        void renderDraw();
    private:
        std::shared_ptr<D3D> m_render_d3d;


        //
        // Render/IA
        //
    private:
        bool renderIaCreateLayout();
        bool renderIaCreateQuad();
    private:
        Microsoft::WRL::ComPtr<ID3D11InputLayout> m_render_ia_layout;
        Microsoft::WRL::ComPtr<ID3D11Buffer> m_render_ia_quad;
        size_t m_render_is_quad_vertexsize{};


        //
        // Render/VS
        //
    private:
        bool renderVsCompile();
    private:
        Microsoft::WRL::ComPtr<ID3DBlob> m_render_vs_blob;
        Microsoft::WRL::ComPtr<ID3D11VertexShader> m_render_vs_shader;


        //
        // Render/Rs
        //
    private:
        bool renderRsCreateState();
    private:
        Microsoft::WRL::ComPtr<ID3D11RasterizerState> m_render_rs_state;

        //
        // Render/PS
        //
    private:
        bool renderPsCompile();
        bool renderPsCreateSampler();
    private:
        Microsoft::WRL::ComPtr<ID3DBlob> m_render_ps_rgb_blob;
        Microsoft::WRL::ComPtr<ID3D11PixelShader> m_render_ps_rgb_shader;

        Microsoft::WRL::ComPtr<ID3DBlob> m_render_ps_nv12_blob;
        Microsoft::WRL::ComPtr<ID3D11PixelShader> m_render_ps_nv12_shader;

        Microsoft::WRL::ComPtr<ID3DBlob> m_render_ps_nv12_mask_blob;
        Microsoft::WRL::ComPtr<ID3D11PixelShader> m_render_ps_nv12_mask_shader;

        Microsoft::WRL::ComPtr<ID3D11SamplerState> m_render_ps_sampler;



        //
        // Render/OM
        //
    private:
        bool renderOmCreateBlendstate();
        bool renderOmCreateRTV();
        bool renderOmClearRTV();
    private:
        Microsoft::WRL::ComPtr<ID3D11BlendState> m_render_om_blendstate;
        Microsoft::WRL::ComPtr<ID3D11RenderTargetView> m_render_om_rtv;

    };
} // namespace WoT::Video
