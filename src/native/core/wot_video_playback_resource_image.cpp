#pragma once

//
// Includes
//

// windows
#include <wrl.h>

// STB
#include "stb_image.h"

// WoT.Video
#include "wot_video_playback_resource_image.h"



//
// Implementation
//

namespace WoT::Video {
    bool PlaybackResourceImage::Init(const std::wstring &path) {
        FILE *file = _wfopen(path.c_str(), L"rb");
        if (!file) {
            return false;
        }

        // Load the image using stbi_load_from_file
        int channels{};
        unsigned char *imageData = stbi_load_from_file(file, &m_size_width, &m_size_height, &channels, 4);
        if (!imageData || m_size_width <= 0 || m_size_height <= 0) {
            fclose(file);
            return false;
        }
        fclose(file);

        m_rs_texture =
            std::move(m_render_d3d->Texture2DCreate(m_size_width, m_size_height, DXGI_FORMAT_R8G8B8A8_UNORM, D3D11_BIND_SHADER_RESOURCE, false, false, imageData));
        if (!m_rs_texture) {
            stbi_image_free(imageData);
            return false;
        }

        stbi_image_free(imageData);

        auto srv = std::move(m_render_d3d->SRVCreate(m_rs_texture));
        if (!srv) {
            return false;
        }

        m_rs_srv.push_back(std::move(srv));
        return true;
    }
} // namespace WoT::Video