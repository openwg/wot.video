// SPDX-License-Identifier: MIT
// Copyright (c) 2019-2024 sentry-native project
// Copyright (c) 2025 Mikhail Paulyshka

//
// Includes
//

// windows
#include <windows.h>

// WoT Video
#include "wot_video_utils.h"



//
// Typedefs
//
typedef HRESULT(WINAPI *pSetThreadDescription)(HANDLE hThread, PCWSTR lpThreadDescription);

const DWORD MS_VC_EXCEPTION = 0x406D1388;

#pragma pack(push, 8)
typedef struct {
    DWORD dwType;     // Must be 0x1000.
    LPCSTR szName;    // Pointer to name (in user addr space).
    DWORD dwThreadID; // Thread ID (-1=caller thread).
    DWORD dwFlags;    // Reserved for future use, must be zero.
} THREADNAME_INFO;
#pragma pack(pop)



//
// Implementation
//

namespace  WoT::Video {
    namespace Utils {
        void SetThreadName(DWORD thread_id, std::string_view thread_name) {
            THREADNAME_INFO threadnameInfo;
            threadnameInfo.dwType = 0x1000;
            threadnameInfo.szName = thread_name.data();
            threadnameInfo.dwThreadID = thread_id;
            threadnameInfo.dwFlags = 0;
#pragma warning(push)
#pragma warning(disable : 6320 6322)
            __try {
                RaiseException(MS_VC_EXCEPTION, 0, sizeof(threadnameInfo) / sizeof(ULONG_PTR), (ULONG_PTR *)&threadnameInfo);
            } __except (EXCEPTION_EXECUTE_HANDLER) {
            }
#pragma warning(pop)
        }


        bool SetThreadName(std::thread &thread, std::wstring_view thread_name) {
            if (thread_name.empty()) {
                return false;
            }

            auto thread_id = GetThreadId(thread.native_handle());
            if (!thread_id) {
                return false;
            }

            // approach 1: Windows 10 1607+
            pSetThreadDescription func = (pSetThreadDescription)GetProcAddress(GetModuleHandleA("kernel32.dll"), "SetThreadDescription");
            if (func) {
                return SUCCEEDED(func(thread.native_handle(), thread_name.data()));
            }

            // approach 2: Windows Vista+ and MSVC debugger
            SetThreadName(GetThreadId(thread.native_handle()), ToString(thread_name));
            return true;
        }


        std::string ToString(std::wstring_view wstrv) {
            if (wstrv.empty()) {
                return "";
            }

            int size_needed = WideCharToMultiByte(CP_UTF8, 0, wstrv.data(), wstrv.size(), nullptr, 0, nullptr, nullptr);
            std::string result(size_needed, 0);
            WideCharToMultiByte(CP_UTF8, 0, wstrv.data(), (int)wstrv.size(), result.data(), size_needed, nullptr, nullptr);
            return result;
        }
    } // namespace Utils
}