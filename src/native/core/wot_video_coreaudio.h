#pragma once

// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Mikhail Paulyshka

// stdlib
#include <cstdint>
#include <vector>

// Windows
#include <Windows.h>

// Core Audio
#include <Audioclient.h>
#include <mmdeviceapi.h>
#include <mmreg.h>
#include <uuids.h>



namespace WoT::Video {
    class CoreAudio {
        //
        // Init
        //
      public:
        CoreAudio() = default;
        ~CoreAudio() = default;
        bool Init(IMMDevice* pDevice);
        bool Deinit();
        [[nodiscard]] bool IsInited() const;
      private:
        bool Init(IAudioClient *pAudioClient);
      private:
        bool _inited = false;


        //
        // IMMDevice
        //
    public:
        IMMDevice* MMDeviceGet();
    private:
        IMMDevice* m_device{};

        //
        // WaveFormat
        //
      public:
        [[nodiscard]] uint32_t WaveFormatChannels() const;
        [[nodiscard]] uint32_t WaveFormatBitsPerSample() const;
        [[nodiscard]] uint32_t WaveFormatFrequency() const;
        [[nodiscard]] bool WaveFormatIsFloat() const;

      private:
        bool waveFormatInit(const WAVEFORMATEX *pFormat);
        void waveFormatDeinit();

      private:
        uint32_t _waveFormat = 0;
        uint32_t _waveFormatChannels = 0;
        uint32_t _waveFormatBits = 0;
        uint32_t _waveFormatFrequency = 0;
    };
} // namespace WoT::Video