// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Mikhail Paulyshka

//
// Includes
//

// stdlib
#include <cassert>

// windows
#include <mfapi.h>

// wot.video
#include "wot_video_video_editor.h"



//
// Implementation
//

namespace WoT::Video {
    VideoEditor::~VideoEditor() { Deinit(); }

    //
    // Init
    //

    bool VideoEditor::Init(std::shared_ptr<VideoPlatform> platform) {
        m_platform = std::move(platform);
        return true;
    }

    bool VideoEditor::Deinit() {
        InputCloseAll();
        OutputClose();
        m_platform.reset();

        return true;
    }


    //
    // Input
    //

    bool VideoEditor::InputOpen(const std::wstring &filepath) {
        bool result = false;

        if (!_input_source.contains(filepath)) {
            VideoSource source;
            if (source.Init(filepath, m_platform->MFGetDeviceManager())) {
                _input_source.emplace(filepath, std::move(source));
                result = true;
            }
        }

        return result;
    }

    bool VideoEditor::InputClose(const std::wstring &filepath) {
        bool result = false;

        if (_input_source.contains(filepath)) {
            result = _input_source.erase(filepath);
        }

        return result;
    }

    bool VideoEditor::InputExists(const std::wstring &filepath) const { return _input_source.contains(filepath); }


    bool VideoEditor::InputCloseAll() {
        _input_source.clear();
        return true;
    }



    //
    // Output
    //

    bool VideoEditor::OutputCreate(const std::wstring &filepath_in, const std::wstring &filepath_out) {
        bool result = false;

        if (_input_source.contains(filepath_in)) {
            result = outputSinkCreate(filepath_out, _input_source[filepath_in]);
        }

        return result;
    }

    bool VideoEditor::OutputClose() {
        bool result = false;

        if (m_output_sink) {
            m_output_sink->NotifyEOS(m_output_sink->GetStreamAudioIndex());
            m_output_sink->NotifyEOS(m_output_sink->GetStreamVideoIndex());
            m_output_sink->Stop();
            m_output_sink.reset();

            result = true;
        }

        return true;
    }

    bool VideoEditor::outputSinkCreate(const std::wstring &output, VideoSource &reader) {

        Microsoft::WRL::ComPtr<IMFMediaType> aud_in{};
        Microsoft::WRL::ComPtr<IMFMediaType> aud_out{};
        Microsoft::WRL::ComPtr<IMFMediaType> vid_in{};
        Microsoft::WRL::ComPtr<IMFMediaType> vid_out{};

        if (reader.GetStreamVideoIndex() != std::numeric_limits<uint32_t>::max()) {
            vid_out = MF::MediaTypeGetVideoTarget(reader.GetStreamNativeMediaType(reader.GetStreamVideoIndex()));
        }

        if (reader.GetStreamAudioIndex() != std::numeric_limits<uint32_t>::max()) {
            aud_in = MF::MediaTypeGetAudioIn(reader.GetStreamNativeMediaType(reader.GetStreamAudioIndex()));
            aud_out = MF::MediaTypeGetAudioOut(reader.GetStreamNativeMediaType(reader.GetStreamAudioIndex()), m_output_audio_bitrate);
        }

        // initialize sink writer
        m_output_sink = std::make_unique<VideoSink>();
        if (!m_output_sink->Init(output, m_platform->MFGetDeviceManager(), aud_in, aud_out, vid_in, vid_out)) {
            m_output_sink.reset();
            return false;;
        }

        // start writing
        if (!m_output_sink->Start()) {
            m_output_sink.reset();
            return false;
        }


        return true;;
    }



    //
    // Operations
    //

    bool VideoEditor::OperationAddFragment(const std::wstring &filepath_in, int64_t time_start, int64_t time_end) {
        // check time_start < time_end
        if (time_end < time_start) {
            return false;
        }

        // get reader
        if (!_input_source.contains(filepath_in)) {
            return false;
        }
        auto reader = _input_source[filepath_in];

        // check duration
        auto duration = reader.GetDuration();
        if (duration < time_start) {
            return false;
        }
        if (time_end == 0LL) {
            time_end = duration;
        }
        if (time_end > duration) {
            time_end = duration;
        }

        // add video fragment
        ResultFragmentAdd result_video{};
        if (reader.GetStreamVideoIndex() != std::numeric_limits<uint32_t>::max() &&
            m_output_sink->GetStreamVideoIndex() != std::numeric_limits<uint32_t>::max()) {
            result_video = operationAddFragmentVideo(reader, *m_output_sink, time_start, time_end);
            if (!result_video.success) {
                return false;
            }
        }

        // add audio fragment
        ResultFragmentAdd result_audio{};
        if (reader.GetStreamAudioIndex() != std::numeric_limits<uint32_t>::max() &&
            m_output_sink->GetStreamAudioIndex() != std::numeric_limits<uint32_t>::max()) {
            result_audio = operationAddFragmentAudio(reader, *m_output_sink, time_start, time_end, result_video);
            if (!result_audio.success) {
                return false;
            }
        }

        return true;
    }

    ResultFragmentAdd VideoEditor::operationAddFragmentVideo(VideoSource &video_source, VideoSink &video_sink, int64_t time_start,
                                                             int64_t time_end) {
        ResultFragmentAdd result{};

        uint32_t stream_idx_in = video_source.GetStreamVideoIndex();
        uint32_t stream_idx_out = video_sink.GetStreamVideoIndex();
        bool is_started = false;

        if (!negotiateVideo(video_source, video_sink)) {
            return result;
        }
        if (!video_source.SetPosition(time_start)) {
            return result;
        }

        while (true) {
            // receive sample
            auto sample_in = video_source.GetSample(stream_idx_in);

            // process sample
            if (sample_in->sample) {
                // stop processing stream on time end
                if (sample_in->timestamp > time_end) {
                    result.success = true;
                    break;
                }

                // update timestamp in
                result.timestamp_in_last = sample_in->timestamp + sample_in->duration;
                if (result.timestamp_in_first < 0LL) {
                    result.timestamp_in_first = sample_in->timestamp;
                }


                // adjust sample time
                if (FAILED(sample_in->sample->SetSampleTime(video_sink.TimestampVideoGet() / 100LL))) {
                    result.success = false;
                    break;
                }

                // set Discontinuity on first sample
                if (!is_started) {
                    if (FAILED(sample_in->sample->SetUINT32(MFSampleExtension_Discontinuity, TRUE))) {
                        result.success = false;
                        break;
                    }
                    is_started = true;
                }

                // write sample
                if (!video_sink.AddSample(sample_in->sample, stream_idx_out)) {
                    result.success = false;
                    break;
                }

                // update timestamp out
                result.timestamp_out_last = video_sink.TimestampVideoGet() + sample_in->duration;
                if (result.timestamp_out_first < 0LL) {
                    result.timestamp_out_first = video_sink.TimestampVideoGet();
                }

                // adjust output timestamp
                video_sink.TimestampVideoSet(video_sink.TimestampVideoGet() + sample_in->duration);
            }

            // process end of stream
            if (sample_in->flags & MF_SOURCE_READERF_ENDOFSTREAM) {
                result.success = true;
                break;
            }
        }

        return result;
    }

    ResultFragmentAdd VideoEditor::operationAddFragmentAudio(VideoSource &video_source, VideoSink &video_sink, int64_t time_start,
                                                             int64_t time_end, ResultFragmentAdd video_fragment) {
        ResultFragmentAdd result{};

        uint32_t stream_idx_in = video_source.GetStreamAudioIndex();
        uint32_t stream_idx_out = video_sink.GetStreamAudioIndex();
        bool is_started = false;

        // adjust time_start and time_end in case of existing video fragment
        if (video_fragment.success) {
            time_start = video_fragment.timestamp_in_first;
            time_end = video_fragment.timestamp_in_last;
        }
        assert(time_end >= time_start);

        if (!negotiateAudio(video_source, video_sink)) {
            return result;
        }
        if (!video_source.SetPosition(time_start)) {
            return result;
        }

        // get info about format
        auto media_type = video_source.GetStreamCurrentMediaType(video_source.GetStreamAudioIndex());

        uint32_t media_type_chans{};
        uint32_t media_type_samplerate{};
        uint32_t media_type_bits{};
        int64_t media_type_sampletime{};
        uint32_t media_type_samplesize{};
        if (!media_type) {
            return result;
        }
        if (FAILED(media_type->GetUINT32(MF_MT_AUDIO_NUM_CHANNELS, &media_type_chans))) {
            return result;
        }
        if (FAILED(media_type->GetUINT32(MF_MT_AUDIO_SAMPLES_PER_SECOND, &media_type_samplerate))) {
            return {};
        }
        if (FAILED(media_type->GetUINT32(MF_MT_AUDIO_BITS_PER_SAMPLE, &media_type_bits))) {
            return {};
        }
        media_type_sampletime = 1'000'000'000 / media_type_samplerate;
        media_type_samplesize = media_type_bits / 8 * media_type_chans;

        while (true) {
            // receive sample
            auto sample_in = video_source.GetSample(stream_idx_in);

            // process sample
            if (sample_in->sample) {
                // stop processing stream on time end
                if (sample_in->timestamp > time_end) {
                    result.success = true;
                    break;
                }

                // sample is outbound of video
                if (video_fragment.success && sample_in->timestamp_end < video_fragment.timestamp_in_first) {
                    continue;
                }

                // get sample buffer
                Microsoft::WRL::ComPtr<IMFMediaBuffer> sample_in_buf{};
                DWORD sample_in_len{};
                if (FAILED(sample_in->sample->ConvertToContiguousBuffer(sample_in_buf.GetAddressOf()))) {
                    break;
                }
                if (FAILED(sample_in_buf->GetCurrentLength(&sample_in_len))) {
                    break;
                }

                auto sample_off_start = 0U;
                auto sample_off_end = sample_in_len / media_type_samplesize;

                // trimming
                // sample end within range
                if (sample_in->timestamp <= time_start && sample_in->timestamp_end >= time_start) {
                    sample_off_start = (time_start - sample_in->timestamp) / media_type_sampletime;
                }
                // sample start withing range
                else if (sample_in->timestamp <= time_end && sample_in->timestamp_end >= time_end) {
                    sample_off_end = (time_end - sample_in->timestamp) / media_type_sampletime;
                }

                // skip sample if start is behind of the end
                if (sample_off_start >= sample_off_end) {
                    continue;
                }

                auto sample_out_duration = (sample_off_end - sample_off_start) * media_type_sampletime;

                // update timestamp in
                result.timestamp_in_last = sample_in->timestamp + sample_off_end * media_type_sampletime;
                if (result.timestamp_in_first < 0LL) {
                    result.timestamp_in_first = sample_in->timestamp + sample_off_start * media_type_sampletime;
                }

                // create out buffer
                uint8_t *sample_in_data{};
                if (FAILED(sample_in_buf->Lock(&sample_in_data, nullptr, nullptr))) {
                    break;
                }
                auto sample_out_buf = MF::BufferCreateMemory(sample_in_data + sample_off_start * media_type_samplesize,
                                                             (sample_off_end - sample_off_start) * media_type_samplesize);
                if (FAILED(sample_in_buf->Unlock())) {
                    break;
                }

                // create out sample
                auto sample_out = MF::SampleCreate(sample_out_buf, video_sink.TimestampAudioGet(), sample_out_duration);

                // set Discontinuity on first sample
                if (!is_started) {
                    if (FAILED(sample_out->SetUINT32(MFSampleExtension_Discontinuity, TRUE))) {
                        result.success = false;
                        break;
                    }
                    is_started = true;
                }

                // write sample
                if (!video_sink.AddSample(sample_out, stream_idx_out)) {
                    result.success = false;
                    break;
                }

                // update timestamp out
                result.timestamp_out_last = video_sink.TimestampAudioGet() + sample_out_duration;
                if (result.timestamp_out_first < 0LL) {
                    result.timestamp_out_first = video_sink.TimestampAudioGet();
                }

                // adjust output timestamp
                video_sink.TimestampAudioSet(video_sink.TimestampAudioGet() + sample_out_duration);
            }

            // process end of stream
            if (sample_in->flags & MF_SOURCE_READERF_ENDOFSTREAM) {
                result.success = true;
                break;
            }
        }

        return result;
    }



    //
    // Video
    //

    bool VideoEditor::negotiateAudio(VideoSource &source, VideoSink &sink) {
        // check audio streams existing
        if (source.GetStreamAudioIndex() == std::numeric_limits<uint32_t>::max() || sink.GetStreamAudioIndex() == std::numeric_limits<uint32_t>::max()) {
            return false;
        }

        auto type_in = MF::MediaTypeGetAudioIn(source.GetStreamNativeMediaType(source.GetStreamAudioIndex()));

        // set source reader partial media type
        if (!source.SetCurrentMediaType(source.GetStreamAudioIndex(), type_in)) {
            return false;
        }

        // return back full media type
        auto mediatype_full =  source.GetStreamCurrentMediaType(source.GetStreamAudioIndex());
        if (!mediatype_full) {
            return false;
        }

        // set full media type as input one for sink
        if (!sink.SetMediaTypeInput(sink.GetStreamAudioIndex(), mediatype_full)) {
            return false;
        }

        return true;
    }

    bool VideoEditor::negotiateVideo(VideoSource &source, VideoSink &sink) {
        bool result = true;
        return result;
    }


} // namespace WoT::Video