// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Andrey Andruschyshyn
// Copyright (c) 2022 Mikhail Paulyshka

#pragma once

// stdlib
#include <cstdint>
#include <string>
#include <utility>

// Windows
#include <wrl.h>

// Direct3D
#include <d3d11.h>

// Media Foundation
#include <mfidl.h>
#include <mfreadwrite.h>



namespace WoT::Video {

    namespace MF {
        //
        // IMFMediaType
        //

        [[nodiscard]] bool MediaTypeEquals(Microsoft::WRL::ComPtr<IMFMediaType> &type_1, Microsoft::WRL::ComPtr<IMFMediaType> &type_2);

        /// <summary>
        /// Creates PCM audio input format
        ///
        /// - sample bitness is forced to 16
        /// </summary>
        /// <param name="frequency">audio frequency, must be 44100 or 48000</param>
        /// <param name="channels">audio channels, must be 1, 2 or 6(5.1)</param>
        /// <note>https://docs.microsoft.com/en-us/windows/win32/medfound/aac-encoder</note>
        /// <returns>WRL pointer to the media type object</returns>
        [[nodiscard]] Microsoft::WRL::ComPtr<IMFMediaType> MediaTypeGetAudioIn(uint32_t frequency, uint32_t channels);
        [[nodiscard]] Microsoft::WRL::ComPtr<IMFMediaType> MediaTypeGetAudioIn(Microsoft::WRL::ComPtr<IMFMediaType> audio_native);

        /// <summary>
        /// Creates AAC audio output format
        ///
        /// - sample bitness is forced to 16
        /// </summary>
        /// <param name="frequency">audio frequency, must be 44100 or 48000, must match input</param>
        /// <param name="channels">audio channels, must be 1, 2 or 6(5.1), must match input</param>
        /// <param name="byterate">bytes per second, must be 12000*8, 16000*8, 20000*8, or 24000*8</param>
        /// <note>https://docs.microsoft.com/en-us/windows/win32/medfound/aac-encoder</note>
        /// <returns>WRL pointer to the media type object</returns>
        [[nodiscard]] Microsoft::WRL::ComPtr<IMFMediaType> MediaTypeGetAudioOut(uint32_t frequency, uint32_t channels, uint32_t bitrate);
        [[nodiscard]] Microsoft::WRL::ComPtr<IMFMediaType> MediaTypeGetAudioOut(Microsoft::WRL::ComPtr<IMFMediaType> audio_native, uint32_t bitrate);
        [[nodiscard]] Microsoft::WRL::ComPtr<IMFMediaType> MediaTypeGetVideoIn(uint32_t width, uint32_t height);
        [[nodiscard]] Microsoft::WRL::ComPtr<IMFMediaType> MediaTypeGetVideoOut(uint32_t width, uint32_t height, uint32_t fps,
                                                                                uint32_t bitrate);
        [[nodiscard]] Microsoft::WRL::ComPtr<IMFMediaType> MediaTypeGetVideoTarget(Microsoft::WRL::ComPtr<IMFMediaType> video_native);



        //
        // IMFMediaBuffer
        //
        Microsoft::WRL::ComPtr<IMFMediaBuffer> BufferCreateMemory(size_t buf_len);
        Microsoft::WRL::ComPtr<IMFMediaBuffer> BufferCreateMemory(const void *buf, size_t buf_len);
        Microsoft::WRL::ComPtr<IMFMediaBuffer> BufferCreateTexture(Microsoft::WRL::ComPtr<ID3D11Texture2D> &texture);



        //
        // IMFSample
        //
        Microsoft::WRL::ComPtr<IMFSample> SampleCreate(Microsoft::WRL::ComPtr<IMFMediaBuffer> &buffer, int64_t timestamp_ns,
                                                       int64_t duration_ns);
        Microsoft::WRL::ComPtr<IMFSample> SampleCopy(Microsoft::WRL::ComPtr<IMFSample> &sample);


    } // namespace MF
} // namespace WoT::Video
