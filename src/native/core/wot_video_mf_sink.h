#pragma once

//
// Includes
//

// stdlib
#include <string>

// windows
#include <wrl.h>
#include <mfreadwrite.h>



//
// Class
//

namespace WoT::Video {
    class VideoSink {
    public:
        bool Init(const std::wstring &filename, IMFDXGIDeviceManager* dev_mgr,
            Microsoft::WRL::ComPtr<IMFMediaType> &aud_in,
            Microsoft::WRL::ComPtr<IMFMediaType> &aud_out,
            Microsoft::WRL::ComPtr<IMFMediaType> &vid_in,
            Microsoft::WRL::ComPtr<IMFMediaType> &vid_out
        );

        bool AddSample(Microsoft::WRL::ComPtr<IMFSample> &sample, uint32_t stream_index);

        [[nodiscard]] Microsoft::WRL::ComPtr<IMFMediaType> GetMediaTypeOutput(uint32_t stream_index);

        [[nodiscard]] uint32_t GetStreamAudioIndex() const;

        [[nodiscard]] uint32_t GetStreamVideoIndex() const;

        [[nodiscard]] uint32_t GetStreamIndex(GUID major_type);

        bool NotifyEOS(uint32_t stream_index);

        bool SetMediaTypeInput(uint32_t stream_index,Microsoft::WRL::ComPtr<IMFMediaType> &media_type);

        bool SendTick(uint32_t stream_index, uint64_t timestamp_ns);

        bool Start();

        bool Stop();

        [[nodiscard]] int64_t TimestampAudioGet() const;

        [[nodiscard]] int64_t TimestampVideoGet() const;

        void TimestampAudioSet(int64_t val);

        void TimestampVideoSet(int64_t val);

    private:
        std::wstring m_filepath{};

        Microsoft::WRL::ComPtr<IMFSinkWriter> m_writer{};

        uint32_t m_audio_idx{std::numeric_limits<uint32_t>::max()};
        uint32_t m_video_idx{std::numeric_limits<uint32_t>::max()};

        int64_t m_timestamp_audio{};
        int64_t m_timestamp_video{};
    };
}