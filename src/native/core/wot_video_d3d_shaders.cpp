// SPDX-License-Identifier: MIT
// Copyright (c) 2016 Microsoft Corporation
// Copyright (c) 2025 Mikhail Paulyshka

// See also:
// https://github.com/microsoft/Windows-universal-samples/blob/main/Samples/HolographicFaceTracking/cpp/Content/Shaders/QuadPixelShaderNV12.hlsl
// https://github.com/microsoft/Windows-universal-samples/blob/main/Samples/HolographicFaceTracking/cpp/Content/Shaders/QuadPixelShaderRGB.hlsl

#pragma once

#include "wot_video_d3d_shaders.h"

namespace WoT::Video {
    namespace D3DShaders {
        const std::string_view PS_Sampler_RGB =
            "Texture2D<float4> rgb_channel     : t0;                                      "
            "SamplerState      default_sampler : s0;                                      "
            "                                                                             "
            "struct PS_INPUT {                                                            "
            "    float4 pos       : SV_POSITION;                                          "
            "    float2 texCoord  : TEXCOORD0  ;                                          "
            "};                                                                           "
            "                                                                             "
            "float4 main(PS_INPUT input) : SV_TARGET                                      "
            "{                                                                            "
            "    return float4(rgb_channel.Sample(default_sampler, input.texCoord));      "
            "}                                                                            ";

        const std::string_view PS_Sampler_NV12 =
            "Texture2D<float>  channel_luma    : t0;                                      "
            "Texture2D<float2> channel_chroma  : t1;                                      "
            "SamplerState      default_sampler : s0;                                      "
            "                                                                             "
            "struct PS_INPUT {                                                            "
            "    float4 pos       : SV_POSITION;                                          "
            "    float2 texCoord  : TEXCOORD0  ;                                          "
            "};                                                                           "
            "                                                                             "
            "static const float3x3 YUVtoRGBCoeffMatrix =                                  "
            "{                                                                            "
            "    1.164383f,  1.164383f, 1.164383f,                                        "
            "    0.000000f, -0.391762f, 2.017232f,                                        "
            "    1.596027f, -0.812968f, 0.000000f                                         "
            "};                                                                           "
            "                                                                             "
            "float3 ConvertYUVtoRGB(float3 yuv)                                           "
            "{                                                                            "
            "    yuv -= float3(0.062745f, 0.501960f, 0.501960f);                          "
            "    yuv = mul(yuv, YUVtoRGBCoeffMatrix);                                     "
            "    return saturate(yuv);                                                    "
            "}                                                                            "
            "                                                                             "
            "float4 main(PS_INPUT input) : SV_TARGET                                      "
            "{                                                                            "
            "    float y = channel_luma.Sample(default_sampler, input.texCoord);          "
            "    float2 uv = channel_chroma.Sample(default_sampler, input.texCoord);      "
            "    return float4(ConvertYUVtoRGB(float3(y, uv)), 1.f);                      "
            "}                                                                            ";

        const std::string_view PS_Sampler_NV12_Mask =
            "Texture2D<float>  channel_luma    : t0;                                      "
            "Texture2D<float2> channel_chroma  : t1;                                      "
            "Texture2D<float2> channel_mask    : t2;                                      "
            "SamplerState      default_sampler : s0;                                      "
            "                                                                             "
            "struct PS_INPUT {                                                            "
            "    float4 pos       : SV_POSITION;                                          "
            "    float2 texCoord  : TEXCOORD0  ;                                          "
            "};                                                                           "
            "                                                                             "
            "static const float3x3 YUVtoRGBCoeffMatrix =                                  "
            "{                                                                            "
            "    1.164383f,  1.164383f, 1.164383f,                                        "
            "    0.000000f, -0.391762f, 2.017232f,                                        "
            "    1.596027f, -0.812968f, 0.000000f                                         "
            "};                                                                           "
            "                                                                             "
            "float3 ConvertYUVtoRGB(float3 yuv)                                           "
            "{                                                                            "
            "    yuv -= float3(0.062745f, 0.501960f, 0.501960f);                          "
            "    yuv = mul(yuv, YUVtoRGBCoeffMatrix);                                     "
            "    return saturate(yuv);                                                    "
            "}                                                                            "
            "                                                                             "
            "float4 main(PS_INPUT input) : SV_TARGET                                      "
            "{                                                                            "
            "    float y     = channel_luma.Sample(default_sampler, input.texCoord);      "
            "    float2 uv   = channel_chroma.Sample(default_sampler, input.texCoord);    "
            "    float alpha = channel_mask.Sample(default_sampler, input.texCoord);      "
            "    return float4(ConvertYUVtoRGB(float3(y, uv)), alpha);                    "
            "}                                                                            ";

         const std::string_view VS_Quad =
            "struct VS_INPUT {                                                            "
            "    float2 pos : POSITION;                                                   "
            "    float2 uv  : TEXCOORD0;                                                  "
            "};                                                                           "
            "                                                                             "
            "struct VS_OUTPUT {                                                           "
            "    float4 pos : SV_POSITION;                                                "
            "    float2 uv  : TEXCOORD0;                                                  "
            "};                                                                           "
            "                                                                             "
            "VS_OUTPUT main(VS_INPUT input) {                                             "
            "    VS_OUTPUT output;                                                        "
            "    output.pos = float4(input.pos, 0.0f, 1.0f);                              "
            "    output.uv  = float2(input.uv.x, 1.0f - input.uv.y);                      "
            "    return output;                                                           "
            "};                                                                           ";

    }
}