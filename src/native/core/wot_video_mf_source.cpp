#pragma once

//
// Includes
//

// stdlib
#include <filesystem>

// windows
#include <mfapi.h>
#include <mfidl.h>
#include <propvarutil.h>

// WoT.Video
#include "wot_video_mf_source.h"

#include "wot_video_logger.h"



//
// Class
//

namespace WoT::Video {

    bool VideoSource::Init(const std::wstring &filepath, IMFDXGIDeviceManager* dev_mgr) {
        if (!std::filesystem::exists(filepath)) {
            return false;
        }

        // set attributes
        Microsoft::WRL::ComPtr<IMFAttributes> attributes;
        if (dev_mgr) {
            if (FAILED(MFCreateAttributes(attributes.GetAddressOf(), 2U))) {
                return false;
            }
            if (FAILED(attributes->SetUINT32(MF_READWRITE_ENABLE_HARDWARE_TRANSFORMS, TRUE))) {
                return false;
            }
            if (FAILED(attributes->SetUnknown(MF_SOURCE_READER_D3D_MANAGER, dev_mgr))) {
                return false;
            }
        }

        // create
        if (FAILED(MFCreateSourceReaderFromURL(filepath.c_str(), attributes.Get(), m_reader.GetAddressOf()))) {
            wot_video_logger::log("VideoSource::Init -> failed to MFCreateSourceReaderFromURL");
            return false;
        }

        // unselect all streams
        auto streamCount = GetStreamCount();
        for (uint32_t idx = 0; idx < streamCount; idx++) {
            if (!SetStreamSelection(idx, FALSE)) {
                wot_video_logger::log("VideoSource::Init -> failed to SetStreamSelection(FALSE)");
                m_reader.Reset();
                return false;
            }
        }

        // Select audio
        auto streams_audio = GetStreamIndex(MFMediaType_Audio);
        if (!streams_audio.empty()) {
            m_audio_idx = streams_audio.front();
             if (!SetStreamSelection(GetStreamAudioIndex(), true)) {
                 wot_video_logger::log("VideoSource::Init -> failed to SetStreamSelection(AUDIO)");
             }
        }
        else {
            wot_video_logger::log("VideoSource::Init -> no audio streams");
        }

        // Select video
        auto streams_video = GetStreamIndex(MFMediaType_Video);
        if (!streams_video.empty()) {
            m_video_idx = streams_video.front();
            if (!SetStreamSelection(GetStreamVideoIndex(), true)) {
                wot_video_logger::log("VideoSource::Init -> failed to SetStreamSelection(VIDEO)");
            }
        }
        else {
            wot_video_logger::log("VideoSource::Init -> no video streams");
        }

        // Set Postion
        if (!SetPosition(0)) {
            wot_video_logger::log("VideoSource::Init -> failed to SetPosition");
        }

        m_filepath = filepath;
        wot_video_logger::log("VideoSource::Init -> OK");
        return true;
    }


    int64_t VideoSource::GetDuration() {
        int64_t result = 0U;

        if (m_reader) {
            PROPVARIANT var{};
            PropVariantInit(&var);

            if (SUCCEEDED(m_reader->GetPresentationAttribute(MF_SOURCE_READER_MEDIASOURCE, MF_PD_DURATION, &var))) {
                result = var.uhVal.QuadPart * 100ULL;
            } else {
                wot_video_logger::log("VideoSource::GetDuration -> failed to GetPresentationAttribute");
            }

            PropVariantClear(&var);
        }

        return result;
    }


    std::unique_ptr<VideoSample> VideoSource::GetSample() { return GetSample(static_cast<DWORD>(MF_SOURCE_READER_ANY_STREAM)); }


    std::unique_ptr<VideoSample> VideoSource::GetSample(uint32_t stream_idx) {
        auto result = std::make_unique<VideoSample>();

        DWORD out_stream_idx_d{};
        DWORD out_flags_d{};
        LONGLONG out_timestamp_ll{};
        LONGLONG out_duration_ll{};

        HRESULT hr = m_reader->ReadSample(stream_idx, 0U, &out_stream_idx_d, &out_flags_d, &out_timestamp_ll, result->sample.GetAddressOf());
        if (SUCCEEDED(hr)){
            result->stream_idx = out_stream_idx_d;
            result->flags = out_flags_d;
            result->timestamp = out_timestamp_ll * 100LL;

            if (result->sample && SUCCEEDED(result->sample->GetSampleDuration(&out_duration_ll))) {
                result->duration = out_duration_ll * 100LL;
                result->timestamp_end = result->timestamp + result->duration;
            }
        } else {
            wot_video_logger::log("VideoSource::GetSample -> failed to ReadSample : ", hr);
            result->sample.Reset();
        }


        return result;
    }


    uint32_t VideoSource::GetStreamCount() {
        uint32_t result = 0U;

        if (m_reader) {
            int selection = false;
            while (SUCCEEDED(m_reader->GetStreamSelection(result, &selection))) {
                result++;
            }
        }

        return result;
    }


    Microsoft::WRL::ComPtr<IMFMediaType> VideoSource::GetStreamCurrentMediaType(uint32_t stream_idx) {
        Microsoft::WRL::ComPtr<IMFMediaType> result;

        if (m_reader) {
            if (FAILED(m_reader->GetCurrentMediaType(stream_idx, result.GetAddressOf()))) {
                result.Reset();
            }
        }

        return result;
    }


    Microsoft::WRL::ComPtr<IMFMediaType> VideoSource::GetStreamNativeMediaType(uint32_t stream_idx) {
        Microsoft::WRL::ComPtr<IMFMediaType> result;
        if (m_reader) {
            if (FAILED(m_reader->GetNativeMediaType(stream_idx, MF_SOURCE_READER_CURRENT_TYPE_INDEX, result.GetAddressOf()))) {
                result.Reset();
            }
        }

        return result;
    }


    uint32_t VideoSource::GetStreamAudioIndex() const {
        return m_audio_idx;
    }


    uint32_t VideoSource::GetStreamVideoIndex() const { return m_video_idx; }

    std::pair<uint32_t, uint32_t> VideoSource::GetVideoDimensions() {
        auto media_type = GetStreamCurrentMediaType(GetStreamVideoIndex());
        if (!media_type) {
            return {};
        }

        uint32_t width{};
        uint32_t height{};
        if (FAILED(MFGetAttributeSize(media_type.Get(), MF_MT_FRAME_SIZE, &width, &height))) {
            return {};
        }

        return {width, height};
    }


    std::vector<uint32_t> VideoSource::GetStreamIndex(GUID major_type) {
        std::vector<uint32_t> result{};

        if (m_reader) {
            auto count = GetStreamCount();
            for (uint32_t idx = 0; idx < count; idx++) {
                auto media_type = GetStreamNativeMediaType(idx);
                if (!media_type) {
                    continue;
                }

                GUID media_type_major{};
                if (FAILED(media_type->GetGUID(MF_MT_MAJOR_TYPE, &media_type_major))) {
                    continue;
                }

                if (media_type_major == major_type) {
                    result.push_back(idx);
                }
            }
        }

        return result;
    }


    bool VideoSource::SetCurrentMediaType(uint32_t stream_idx, Microsoft::WRL::ComPtr<IMFMediaType> &media_type) {
        auto hr = m_reader->SetCurrentMediaType(stream_idx, nullptr, media_type.Get());
        if (FAILED(hr)) {
            wot_video_logger::log("VideoSource::SetCurrentMediaType -> failed to set SetCurrentMediaType: ", hr);
            return false;
        }
        return true;
    }


    bool VideoSource::SetPosition(int64_t position_ns) {
        if (!m_reader) {
            return false;
        }

        if (position_ns > GetDuration()) {
            return false;
        }

        PROPVARIANT pos{};
        InitPropVariantFromInt64(position_ns / 100ULL, &pos);
        bool result = SUCCEEDED(m_reader->SetCurrentPosition(GUID_NULL, pos));
        PropVariantClear(&pos);

        return result;
    }


    bool VideoSource::SetStreamSelection(uint32_t stream_idx, bool selected) {
        if (stream_idx == std::numeric_limits<uint32_t>::max()) {
            wot_video_logger::log("VideoSource::SetStreamSelection -> invalid stream index");
            return false;
        }

        if (FAILED(m_reader->SetStreamSelection(stream_idx, selected))){
            wot_video_logger::log("VideoSource::SetStreamSelection -> failed to SetStreamSelection");
            return false;
        }

        return true;
    }
} // namespace WoT::Video
