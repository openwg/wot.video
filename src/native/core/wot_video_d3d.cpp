// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Mikhail Paulyshka

// stdlib
#include <memory>

// wot.video
#include "wot_video_d3d.h"



namespace WoT::Video {

    //
    // Init
    //

    D3D::D3D(XFW::Native::Hooks::HookmanagerD3D &hookmanager_d3d) : m_hooks_mgr(hookmanager_d3d) {}
    D3D::~D3D() {
        hooksDeinit();
        Deinit();
    }

    bool D3D::Init() { return hooksInit(); }

    bool D3D::Inited() const { return _swapchain && _device && _deviceContext; }

    void D3D::Deinit() {
        hooksDeinit();
        _deviceContext = nullptr;
        _device = nullptr;
        _swapchain = nullptr;
    }

    //
    // Handlers
    //

    void D3D::HandlerDeviceRegister(const std::string &name, std::function<void()> func) { m_handler_device[name] = std::move(func); }

    void D3D::HandlerDeviceUnregister(const std::string &name) { m_handler_device.erase(name); }

    void D3D::HandlerPresentRegister(const std::string &name, std::function<void()> func) { m_handler_present[name] = std::move(func); }

    void D3D::HandlerPresentUnregister(const std::string &name) { m_handler_present.erase(name); }

    void D3D::HandlerResizeBeforeRegister(const std::string &name, std::function<void()> func) { m_handler_resize_before[name] = std::move(func); }

    void D3D::HandlerResizeBeforeUnregister(const std::string &name) { m_handler_resize_before.erase(name); }

    void D3D::HandlerSwapchainRegister(const std::string &name, std::function<void()> func) { m_handler_swapchain[name] = std::move(func); }

    void D3D::HandlerSwapchainUnregister(const std::string &name) { m_handler_swapchain.erase(name);  }

    void D3D::handlerDeviceExecute() {
        for (auto &pair : m_handler_device) {
            pair.second();
        }
    }

    void D3D::handlerPresentExecute() {
        for (auto &pair : m_handler_present) {
            pair.second();
        }
    }
    void D3D::handlerResizeBeforeExecute() {
        for (auto &pair : m_handler_resize_before) {
            pair.second();
        }
    }

    void D3D::handlerSwapchainExecute() {
        for (auto &pair : m_handler_swapchain) {
            pair.second();
        }
    }



    //
    // Hooks
    //

    bool D3D::hooksInit() {
        m_hooks_mgr.init();

        m_hooks_mgr.IDXGISwapChain_Present_register("wot_video_d3d", XFW::Native::Hooks::HookPlace::Before,
                                                    [this](IDXGISwapChain *swapChain, UINT, UINT) { hooksDxgiSwapchainPresent(swapChain); });

        m_hooks_mgr.IDXGISwapChain_ResizeBuffers_register(
           "wot_video_d3d", XFW::Native::Hooks::HookPlace::Before,
           [this](IDXGISwapChain *swapChain, UINT, UINT, UINT, DXGI_FORMAT, UINT) { hooksDxgiSwapchainResizeBuffersBefore(); });

        m_hooks_mgr.IDXGISwapChain_ResizeBuffers_register(
            "wot_video_d3d", XFW::Native::Hooks::HookPlace::After,
            [this](IDXGISwapChain *swapChain, UINT, UINT, UINT, DXGI_FORMAT, UINT) { hooksDxgiSwapchainResizeBuffersAfter(swapChain); });

        return true;
    }

    void D3D::hooksDeinit() {
        m_hooks_mgr.IDXGISwapChain_Present_unregister("wot_video_d3d", XFW::Native::Hooks::HookPlace::Before);
        m_hooks_mgr.IDXGISwapChain_ResizeBuffers_unregister("wot_video_d3d", XFW::Native::Hooks::HookPlace::Before);
        m_hooks_mgr.IDXGISwapChain_ResizeBuffers_unregister("wot_video_d3d", XFW::Native::Hooks::HookPlace::After);
    }

    void D3D::hooksDxgiSwapchainPresent(IDXGISwapChain *swapChain) {
        if (swapChain != _swapchain) {
            swapchainUpdate(swapChain);
        }
        handlerPresentExecute();
    }

    void D3D::hooksDxgiSwapchainResizeBuffersBefore() {
        handlerResizeBeforeExecute();
    }

    void D3D::hooksDxgiSwapchainResizeBuffersAfter(IDXGISwapChain *swapchain) {
        swapchainUpdate(swapchain);
    }



    //
    // SwapChain
    //

    Microsoft::WRL::ComPtr<ID3D11Resource> D3D::SwapchainGetBackbuffer() {
        Microsoft::WRL::ComPtr<ID3D11Resource> result;

        if (_swapchain) {
            _swapchain->GetBuffer(0, IID_PPV_ARGS(&result));
        }

        return result;
    }

    Microsoft::WRL::ComPtr<ID3D11Texture2D> D3D::SwapchainGetBackbufferCopy(bool cpuAccess) {
        // get backbuffer
        auto backbuffer = SwapchainGetBackbuffer();
        if (!backbuffer) {
            return {};
        }

        // create texture
        auto texture = Texture2DCreate(SwapchainGetWidth(), SwapchainGetHeight(), SwapchainGetFormat(), 0, cpuAccess, false);
        if (!texture) {
            return {};
        }

        // copy
        ResourceCopy(texture.Get(), backbuffer.Get());

        return texture;
    }

    std::vector<uint8_t> D3D::SwapchainGetBackbufferPixels() {
        std::vector<uint8_t> result;

        // get texture
        auto texture = SwapchainGetBackbufferCopy(true);
        if (!texture) {
            return result;
        }


        // get texture info
        D3D11_TEXTURE2D_DESC texture_desc{};
        texture->GetDesc(&texture_desc);

        // map texture
        D3D11_MAPPED_SUBRESOURCE texture_mapped{};
        if (FAILED(_deviceContext->Map(texture.Get(), 0U, D3D11_MAP_READ, 0U, &texture_mapped))) {
            return result;
        }
        auto texture_mapped_data = reinterpret_cast<const uint8_t *>(texture_mapped.pData);

        // copy data and remove alpha
        size_t size_pixel = 4U;
        size_t size_row = texture_desc.Width * size_pixel;
        size_t size_image = texture_desc.Width * texture_desc.Height * size_pixel;

        result.resize(size_image);
        for (size_t y = 0; y < texture_desc.Height; y++) {
            std::memcpy(&result[size_row * y], &texture_mapped_data[texture_mapped.RowPitch * y], std::min<size_t>(size_row, texture_mapped.RowPitch));

            for (size_t x = 0; x < size_row; x += size_pixel) {
                // remove alpha
                result[size_row * y + x + 3] = 0xFF;

                // swap B<->G channels if needed
                if (texture_desc.Format == DXGI_FORMAT_B8G8R8A8_UNORM || texture_desc.Format == DXGI_FORMAT_B8G8R8X8_UNORM ||
                    texture_desc.Format == DXGI_FORMAT_B8G8R8A8_TYPELESS || texture_desc.Format == DXGI_FORMAT_B8G8R8A8_UNORM_SRGB ||
                    texture_desc.Format == DXGI_FORMAT_B8G8R8X8_TYPELESS) {
                    std::swap(result[size_row * y + x + 0], result[size_row * y + x + 2]);
                }
            }
        }

        // unmap texture
        _deviceContext->Unmap(texture.Get(), 0U);

        return result;
    }

    DXGI_FORMAT D3D::SwapchainGetFormat() const { return m_swapchain_desc.BufferDesc.Format; }

    uint32_t D3D::SwapchainGetHeight() const { return m_swapchain_desc.BufferDesc.Height; }

    uint32_t D3D::SwapchainGetWidth() const { return m_swapchain_desc.BufferDesc.Width; }

    void D3D::swapchainUpdate(IDXGISwapChain *swapchain) {
        bool swapchain_changed{};

        // IDXGISwapChain
        if (_swapchain != swapchain) {
            _swapchain = swapchain;
            deviceUpdate();
            swapchain_changed = true;
        }

        DXGI_SWAP_CHAIN_DESC swapchain_desc{};
        if (FAILED(_swapchain->GetDesc(&swapchain_desc))) {
            Deinit();
        }

        if (memcmp(&swapchain_desc, &m_swapchain_desc, sizeof(m_swapchain_desc)) != 0) {
            swapchain_changed = true;
        }

        if (swapchain_changed) {
            handlerSwapchainExecute();
        }
    }



    //
    // ID3D11Device
    //

    ID3D11Device *D3D::DeviceGet() { return _device; }

    void D3D::deviceUpdate() {
        // ID3D11Device
        auto device_old = _device;
        if (FAILED(_swapchain->GetDevice(IID_PPV_ARGS(&_device)))) {
            Deinit();
            return;
        }
        _device->Release();
        if (device_old != _device) {
            // ID3D11DeviceContext
            _device->GetImmediateContext(&_deviceContext);
            _deviceContext->Release();

            // ID3D10Multithread
            Microsoft::WRL::ComPtr<ID3D10Multithread> multithread;
            if (FAILED(_device->QueryInterface(IID_PPV_ARGS(&multithread)))) {
                Deinit();
                return;
            }
            multithread->SetMultithreadProtected(TRUE);

            handlerDeviceExecute();
        }
    }



    //
    // ID3D11DeviceContext
    //

    ID3D11DeviceContext *D3D::DeviceGetContext() { return _deviceContext; }



    //
    // ID3D11Resource
    //

    bool D3D::ResourceCopy(ID3D11Resource *destination, ID3D11Resource *source, bool multisampled, DXGI_FORMAT format) {
        bool result = false;

        if (_deviceContext && destination && source) {
            if (multisampled) {
                _deviceContext->ResolveSubresource(destination, 0U, source, 0U, format);
            } else {
                _deviceContext->CopyResource(destination, source);
            }

            result = true;
        }

        return result;
    }

    bool D3D::ResourceCopy(Microsoft::WRL::ComPtr<ID3D11Resource> &destination, Microsoft::WRL::ComPtr<ID3D11Resource> &source, bool multisampled,
                           DXGI_FORMAT format) {
        return ResourceCopy(destination.Get(), source.Get(), multisampled, format);
    }

    bool D3D::ResourceCopy(Microsoft::WRL::ComPtr<ID3D11Texture2D> &destination, Microsoft::WRL::ComPtr<ID3D11Texture2D> &source, bool multisampled,
                           DXGI_FORMAT format) {
        return ResourceCopy(destination.Get(), source.Get(), multisampled, format);
    }



    //
    // Texture2D
    //

    Microsoft::WRL::ComPtr<ID3D11Texture2D> D3D::Texture2DCreate(uint32_t width, uint32_t height, DXGI_FORMAT format, UINT bind_flags, bool cpu_read,
                                                                 bool cpu_write, void *initial_data) {
        D3D11_TEXTURE2D_DESC desc{
            .Width = width,
            .Height = height,
            .MipLevels = 1U,
            .ArraySize = 1U,
            .Format = format,
            .SampleDesc =
                {
                    .Count = 1,
                    .Quality = 0,
                },
            .Usage = D3D11_USAGE_DEFAULT,
            .BindFlags = bind_flags,
            .CPUAccessFlags = 0U,
            .MiscFlags = 0U,
        };

        // CPUAccessFlags
        auto cpu_access = 0U;
        if (cpu_read) {
            cpu_access |= D3D11_CPU_ACCESS_READ;
        }
        if (cpu_write) {
            cpu_access |= D3D11_CPU_ACCESS_WRITE;
        }
        desc.CPUAccessFlags = cpu_access;

        // Usage
        if (cpu_write) {
            desc.Usage = D3D11_USAGE_DYNAMIC;
        } else if (cpu_read) {
            desc.Usage = D3D11_USAGE_STAGING;
        }

        auto format_width = 0;
        if (format == DXGI_FORMAT_R8G8B8A8_UNORM) {
            format_width = 4;
        }
        else if (format == DXGI_FORMAT_R8_UNORM) {
            format_width = 1;
        }

        if (initial_data) {
            D3D11_SUBRESOURCE_DATA initData = {
                .pSysMem = initial_data,
                .SysMemPitch = width * format_width,
                .SysMemSlicePitch = 0U,
            };

            return Texture2DCreate(desc, initData);
        }
        return Texture2DCreate(desc);
    }


    Microsoft::WRL::ComPtr<ID3D11Texture2D> D3D::Texture2DCreate(D3D11_TEXTURE2D_DESC &texture_desc) {
        Microsoft::WRL::ComPtr<ID3D11Texture2D> result;
        if (FAILED(_device->CreateTexture2D(&texture_desc, nullptr, result.GetAddressOf()))) {
            return {};
        }
        return result;
    }


    Microsoft::WRL::ComPtr<ID3D11Texture2D> D3D::Texture2DCreate(D3D11_TEXTURE2D_DESC &texture_desc, D3D11_SUBRESOURCE_DATA &init_data) {
        Microsoft::WRL::ComPtr<ID3D11Texture2D> result;
        if (FAILED(_device->CreateTexture2D(&texture_desc, &init_data, result.GetAddressOf()))) {
            return {};
        }
        return result;
    }



    //
    // ID3D11ShaderResourceView
    //

    Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> D3D::SRVCreate(const Microsoft::WRL::ComPtr<ID3D11Texture2D> &texture) {
        Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> result;
        if (FAILED(_device->CreateShaderResourceView(texture.Get(), nullptr, result.GetAddressOf()))) {
            return {};
        }
        return result;
    }

    Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> D3D::SRVCreate(const Microsoft::WRL::ComPtr<ID3D11Texture2D> &texture, DXGI_FORMAT format) {
        Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> result;

        auto const desc = CD3D11_SHADER_RESOURCE_VIEW_DESC(
                   texture.Get(),
                   D3D11_SRV_DIMENSION_TEXTURE2D,
                   format
        );

        if (FAILED(_device->CreateShaderResourceView(texture.Get(), &desc, result.GetAddressOf()))) {
            return {};
        }

        return result;
    }
} // namespace WoT::Video
