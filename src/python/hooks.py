"""
SPDX-License-Identifier: MIT
Copyright (c) 2023 Andrii Andrushchyshyn
Copyright (c) 2023 Mikhail Paulyshka
"""

#
# Imports
#
import thread
from Avatar import PlayerAvatar
from BattleReplay import BattleReplay
from PlayerEvents import g_playerEvents
from ReplayEvents import g_replayEvents

from . import g_video_config as config, g_video_native_client
from .utils import override

global __record_active
__record_active = False

def record_start():

	if not config.enabled:
		return

	global __record_active
	if __record_active:
		return
	__record_active = True

	def threaded(native, config):
		if native.video_active():
			return
		#native.video_start(config.filepath_video, config.width, config.height,
		#	config.fps, config.bitrate_audio, config.bitrate_video)
		cookie_1 = native.playback_add_image(u'G:\\oia-uia.png')
		if cookie_1 > 0:
			native.playback_set_position(cookie_1, 100, 150)

		cookie_2 = native.playback_add_image(u'G:\\bober.png')
		if cookie_2 > 0:
			native.playback_set_position(cookie_2, 400, 250)

		cookie_3 = native.playback_add_image(u'G:\\grad.png')
		if cookie_3 > 0:
			native.playback_set_position(cookie_3, 600, 350)

		cookie_4 = native.playback_add_video(u'G:\\gb1bNciTel2jI8Rz.mp4')
		if cookie_4 > 0:
			native.playback_set_position(cookie_4, 700, 450)
	


	thread.start_new_thread(threaded, (g_video_native_client, config, ))

def record_stop():

	global __record_active
	if not __record_active:
		return
	__record_active = False

	if not config.enabled:
		return

	def threaded(native, config):
		if not native.video_active():
			return
		native.video_stop()

	thread.start_new_thread(threaded, (g_video_native_client, config, ))


def onAvatarBecomeNonPlayer(*a, **kw):
	if not config.enabled:
		return
	record_stop()

def onReplayTerminated(*a, **kw):
	if not config.enabled:
		return
	record_stop()

def hooked_onReplayFinished(baseMethod, baseObject):
	if config.enabled:
		record_stop()
	base = baseMethod(baseObject)
	return base

def hooked_onSpaceLoaded(baseMethod, baseObject):
	base = baseMethod(baseObject)
	record_start()
	return base

def hooks_init():

	override(BattleReplay, "onReplayFinished")(hooked_onReplayFinished)
	override(PlayerAvatar, "onSpaceLoaded")(hooked_onSpaceLoaded)

	g_playerEvents.onAvatarBecomeNonPlayer += onAvatarBecomeNonPlayer
	g_replayEvents.onReplayTerminated += onReplayTerminated
