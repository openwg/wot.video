// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Andrey Andruschyshyn
// Copyright (c) 2022, 2023, 2025 Mikhail Paulyshka

// stdlib
#include <memory>

// python
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

// AK
#include <AK/SoundEngine/Common/AkSoundEngine.h>

// wot.video
#include "wot_video_mf_platform.h"
#include "wot_video_playback.h"
#include "wot_video_screenshot.h"
#include "wot_video_video_capture.h"
#include "wot_video_video_editor.h"


namespace WoT::Video {
    //
    // CPP
    //

    class wot_video_client {
        //
        // ctor/dtor
        //
      public:
        wot_video_client() = default;
        ~wot_video_client() = default;


        //
        // Init
        //
      public:
        bool Init() {
            IMMDevice *device{};
            AkUInt32 device_id{};
            if (!AK::GetWindowsDevice(-1, device_id, &device)) {
                return false;
            }

            m_coreaudio = std::make_shared<CoreAudio>();
            if (!m_coreaudio->Init(device)) {
                return false;
            }


            m_d3d = std::make_shared<D3D>(XFW::Native::Hooks::HookmanagerD3D::instance());
            if (!m_d3d->Init()) {
                return false;
            }

            m_video_platform = std::make_shared<VideoPlatform>();
            if (!m_video_platform->Init(m_d3d)) {
                return false;
            }

            return true;
        }

        bool Deinit() {
            if (_capture_video) {
                _capture_video->Deinit();
                _capture_video.reset();
            }

            if (_capture_screenshot) {
                _capture_screenshot->Deinit();
                _capture_screenshot.reset();
            }

            if (m_video_platform) {
                m_video_platform->Deinit();
                m_video_platform.reset();
            }

            if (m_coreaudio) {
                m_coreaudio->Deinit();
                m_coreaudio.reset();
            }

            if (m_d3d) {
                m_d3d->Deinit();
                m_d3d.reset();
            }
            
            return true;
        }

        //
        // Playback
        //

    public:
        int PlaybackAddImage(const std::wstring &filename) {
            if (!playbackInit()) {
                return 0;
            }
            return _playback_video->ResourceAddImage(filename);
        }

        int PlaybackAddVideo(const std::wstring &filename) {
            if (!playbackInit()) {
                return 0;
            }
            return _playback_video->ResourceAddVideo(filename);
        }

        bool PlaybackSetMask(int cookie, const std::wstring &filename) {
            if (!playbackInit()) {
                return false;
            }
            return _playback_video->ResourceMaskSet(cookie, filename);
        }

        bool PlaybackSetPosition(int cookie, int x, int y) {
            if (!playbackInit()) {
                return false;
            }
            return _playback_video->ResourcePositionSet(cookie, x, y);
        }

        bool PlaybackSetSize(int cookie, int width, int height) {
            if (!playbackInit()) {
                return false;
            }
            return _playback_video->ResourceSizeSet(cookie, width, height);
        }

        bool PlaybackSetVisibility(int cookie, bool val) {
            if (!playbackInit()) {
                return false;
            }
            return _playback_video->ResourceVisiblitySet(cookie, val);
        }

        bool PlaybackSetVisibilityOnFinish(int cookie, bool val) {
            if (!playbackInit()) {
                return false;
            }
            return _playback_video->ResourceVisiblityOnFinishSet(cookie, val);
        }

    private:
        bool playbackInit() {
            if (!_playback_video) {
                _playback_video = std::make_unique<PlaybackVideo>();
                if (!_playback_video->Init(m_coreaudio, m_d3d, m_video_platform)) {
                    _playback_video.reset();
                    return false;
                }
            }
            return true;
        }

        //
        // Screenshot
        //
      public:
        bool Screenshot(const std::wstring &filename) { 
            if (!_capture_screenshot) {
                _capture_screenshot = std::make_unique<CaptureScreenshot>(XFW::Native::Hooks::HookmanagerD3D::instance());
                if (!_capture_screenshot->Init(m_d3d)) {
                    _capture_screenshot.reset();
                    return false;
                }
            }
            return _capture_screenshot->Capture(filename); 
        }


        //
        // Video
        //
      public:
        bool VideoActive() { 
            return _capture_video && _capture_video->CaptureActive(); 
        }

        bool VideoStart(const std::wstring &filename, uint32_t width, uint32_t height, uint32_t fps, uint32_t bitrate_audio,
                        uint32_t bitrate_video) {
            if (!_capture_video) {
                _capture_video = std::make_unique<CaptureVideo>(XFW::Native::Hooks::HookmanagerD3D::instance());
                if (!_capture_video->Init(m_coreaudio, m_d3d, m_video_platform)) {
                    _capture_video.reset();
                    return false;
                }
            }
            return _capture_video->CaptureStart(filename, width, height, fps, bitrate_audio, bitrate_video);
        }

        bool VideoStop() {
            if (!_capture_video) {
                return false;
            }

            return _capture_video->CaptureStop();
        }

        uint64_t VideoTimestamp() const {
            if (!_capture_video) {
                return 0;
            }
            return _capture_video->TimestampGet(); 
        }


        //
        // Trim
        //
        bool Trim(const std::wstring &filename_in, const std::wstring &filename_out,
                  const std::vector<std::pair<int64_t, int64_t>> &trim_ranges) {
            VideoEditor editor{};
            if (!editor.Init(m_video_platform)) {
                return false;
            }

            if (!editor.InputOpen(filename_in)) {
                return false;
            }

            if (!editor.OutputCreate(filename_in, filename_out)) {
                return false;
            }

            for (const auto &segment : trim_ranges) {
                if (!editor.OperationAddFragment(filename_in, segment.first, segment.second)) {
                    return false;
                }
            }

            if (!editor.OutputClose()) {
                return false;
            }

            if (!editor.InputCloseAll()) {
                return false;
            }

            if (!editor.Deinit()) {
                return false;
            }

            return true;
        }



        //
        // Components
        //
      private:
        std::shared_ptr<CoreAudio> m_coreaudio;
        std::shared_ptr<D3D> m_d3d;
        std::shared_ptr<VideoPlatform> m_video_platform;
        std::unique_ptr<CaptureVideo> _capture_video;
        std::unique_ptr<CaptureScreenshot> _capture_screenshot;
        std::unique_ptr<PlaybackVideo> _playback_video;
    };



    /////////////////////////////////////////////
    /////////////////////////////////////////////
    /////////////////////////////////////////////



    //
    // Pybind11
    //
    PYBIND11_MODULE(WoT_Video, m) {
        m.doc() = "WoT Video module";

        pybind11::class_<wot_video_client>(m, "WoT_Video_Client")
            .def(pybind11::init<>())

            .def("init", &wot_video_client::Init)
            .def("deinit", &wot_video_client::Deinit)

            .def("screenshot", &wot_video_client::Screenshot, pybind11::call_guard<pybind11::gil_scoped_release>())

            .def("video_active", &wot_video_client::VideoActive)
            .def("video_start", &wot_video_client::VideoStart, pybind11::call_guard<pybind11::gil_scoped_release>())
            .def("video_stop", &wot_video_client::VideoStop, pybind11::call_guard<pybind11::gil_scoped_release>())
            .def("video_timestamp", &wot_video_client::VideoTimestamp)

            .def("video_trim", &wot_video_client::Trim, pybind11::call_guard<pybind11::gil_scoped_release>())

            .def("playback_add_image", &wot_video_client::PlaybackAddImage, pybind11::call_guard<pybind11::gil_scoped_release>())
            .def("playback_add_video", &wot_video_client::PlaybackAddVideo, pybind11::call_guard<pybind11::gil_scoped_release>())
            .def("playback_set_mask", &wot_video_client::PlaybackSetMask, pybind11::call_guard<pybind11::gil_scoped_release>())
            .def("playback_set_position", &wot_video_client::PlaybackSetPosition, pybind11::call_guard<pybind11::gil_scoped_release>())
            .def("playback_set_size", &wot_video_client::PlaybackSetSize, pybind11::call_guard<pybind11::gil_scoped_release>())
            .def("playback_set_visibility", &wot_video_client::PlaybackSetVisibility, pybind11::call_guard<pybind11::gil_scoped_release>())
            .def("playback_set_visibility_on_finish", &wot_video_client::PlaybackSetVisibilityOnFinish, pybind11::call_guard<pybind11::gil_scoped_release>());
    }
} // namespace WoT::Video
