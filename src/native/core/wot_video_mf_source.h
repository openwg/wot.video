#pragma once

//
// Includes
//

// stdlib
#include <cstdint>
#include <string>
#include <vector>

// windows
#include <mfidl.h>
#include <mfobjects.h>
#include <mfreadwrite.h>
#include <wrl.h>

// WoT.Video
#include "deprecated/stb.h"
#include "wot_video_mf_sample.h"



//
// Class
//

namespace WoT::Video {
    class VideoSource {
      public:
        [[nodiscard]] bool Init(const std::wstring &filepath, IMFDXGIDeviceManager* mf_dxgi);

        [[nodiscard]] int64_t GetDuration();

        [[nodiscard]] std::unique_ptr<VideoSample> GetSample();

        [[nodiscard]] std::unique_ptr<VideoSample> GetSample(uint32_t stream_idx);

        [[nodiscard]] uint32_t GetStreamCount();

        [[nodiscard]] Microsoft::WRL::ComPtr<IMFMediaType> GetStreamCurrentMediaType(uint32_t stream_idx);

        [[nodiscard]] Microsoft::WRL::ComPtr<IMFMediaType> GetStreamNativeMediaType(uint32_t stream_idx);

        [[nodiscard]] uint32_t GetStreamAudioIndex() const;

        [[nodiscard]] uint32_t GetStreamVideoIndex() const;

        [[nodiscard]] std::pair<uint32_t, uint32_t> GetVideoDimensions();

        [[nodiscard]] std::vector<uint32_t> GetStreamIndex(GUID major_type);

        bool SetCurrentMediaType(uint32_t stream_idx, Microsoft::WRL::ComPtr<IMFMediaType> &media_type);

        bool SetPosition(int64_t position_ns);

        bool SetStreamSelection(uint32_t stream_idx, bool selected);


      private:
        std::wstring m_filepath{};
        Microsoft::WRL::ComPtr<IMFSourceReader> m_reader;
        uint32_t m_audio_idx = std::numeric_limits<uint32_t>::max();
        uint32_t m_video_idx = std::numeric_limits<uint32_t>::max();
    };
} // namespace WoT::Video
