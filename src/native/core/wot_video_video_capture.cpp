// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Andrey Andruschyshyn
// Copyright (c) 2022 Mikhail Paulyshka

// stdlib
#include <algorithm>
#include <chrono>
#include <functional>

// windows
#include <mfapi.h>

// wot.video
#include "wot_video_video_capture.h"



//
// Implementation
//

using namespace std::placeholders;

namespace WoT::Video {
    //
    // Init
    //
    CaptureVideo::CaptureVideo(XFW::Native::Hooks::HookmanagerD3D &hookmanager_d3d): _hooks_coreaudio(XFW::Native::Hooks::HookmanagerCoreaudio::instance()), _hooks_d3d(hookmanager_d3d) {}

    CaptureVideo::~CaptureVideo() { Deinit(); }

    bool CaptureVideo::Init(std::shared_ptr<CoreAudio> coreaudio, std::shared_ptr<D3D> d3d, std::shared_ptr<VideoPlatform> platform) {
        if (!coreaudio) {
            return false;
        }
        m_audio_core = std::move(coreaudio);
        m_d3d = std::move(d3d);
        m_mf_platform = std::move(platform);

        hooksD3DInit();
        hooksCoreaudioInit();

        _inited = true;
        return true;
    }

    bool CaptureVideo::Deinit() {
        _inited = false;

        // unhook
        hooksCoreaudioDeinit();
        hooksD3DDeinit();

        // stop video
        CaptureStop();

        // clear resampler
        audioResamplerClear();

        // reset components
        m_audio_core.reset();
        m_mf_platform.reset();
        m_d3d.reset();

        return true;
    }



    //
    // MF
    //

    void CaptureVideo::mfStop() {
        if (m_mf_writer) {
            m_mf_writer->NotifyEOS(m_mf_writer->GetStreamAudioIndex());
            m_mf_writer->NotifyEOS(m_mf_writer->GetStreamVideoIndex());
            m_mf_writer->Stop();
        }
        m_mf_writer.reset();
    }



    //
    // Capture
    //

    bool CaptureVideo::CaptureActive() const { return _capture_active.test(); }

    bool CaptureVideo::CaptureStart(const std::wstring &filename, uint32_t width, uint32_t height, uint32_t fps, uint32_t bitrate_audio,
                                    uint32_t bitrate_video) {
        // check if already started
        if (CaptureActive()) {
            return false;
        }

        // check if valid filename
        if (filename.empty()) {
            return false;
        }

        // fixup width/height
        width = width ? width : m_d3d->SwapchainGetWidth();
        height = height ? height : m_d3d->SwapchainGetHeight();

        // init time
        timestampClear();

        // init video auxillary variables
        imageStart(fps);
        auto vid_in = MF::MediaTypeGetVideoIn(m_d3d->SwapchainGetWidth(), m_d3d->SwapchainGetHeight());
        auto vid_out = MF::MediaTypeGetVideoOut(width, height, fps, bitrate_video);
        if (!vid_in || !vid_out) {
            return false;
        }

        // init audio auxillary variables
        Microsoft::WRL::ComPtr<IMFMediaType> aud_in;
        Microsoft::WRL::ComPtr<IMFMediaType> aud_out;
        if (AudioSupported()) {
            audioStart();
            aud_in = MF::MediaTypeGetAudioIn(_audio_frequency, _audio_channels);
            aud_out = MF::MediaTypeGetAudioOut(_audio_frequency, _audio_channels, bitrate_audio);
        }

        // create sink writer
        m_mf_writer = std::make_unique<VideoSink>();
        if (!m_mf_writer->Init(filename, m_mf_platform->MFGetDeviceManager(), aud_in, aud_out, vid_in, vid_out)) {
            CaptureStop();
            return false;
        }
        if (!m_mf_writer->Start()) {
            CaptureStop();
            return false;
        }

        _capture_active.test_and_set();
        return true;
    }

    bool CaptureVideo::CaptureStop() {
        _capture_active.clear();

        mfStop();
        audioStop();
        imageStop();
        timestampClear();

        return true;
    }



    //
    // Audio
    //

    bool CaptureVideo::AudioSupported() const { return m_audio_core->WaveFormatFrequency() >= 44100; }

    void CaptureVideo::audioStart() {
        _audio_buffer = nullptr;

        // channels
        switch (m_audio_core->WaveFormatChannels()) {
        case 1:
            _audio_channels = 1;
            break;
        case 2:
        default:
            _audio_channels = 2;
            break;
        }

        // frequency
        if (m_audio_core->WaveFormatFrequency() % 44100 == 0U) {
            _audio_frequency = 44100;
        } else {
            _audio_frequency = 48000;
        }

        if (m_audio_core->WaveFormatFrequency() != _audio_frequency) {
            audioResamplerInit(m_audio_core->WaveFormatChannels());
            _audio_resampler = src_new(SRC_SINC_MEDIUM_QUALITY, m_audio_core->WaveFormatChannels(), nullptr);
        }
    }

    void CaptureVideo::audioStop() {
        _audio_buffer = nullptr;
        _audio_channels = 0U;
        _audio_frequency = 0U;
    }

    void CaptureVideo::audioResamplerInit(size_t channels) {
        audioResamplerClear();
        _audio_resampler = src_new(SRC_SINC_MEDIUM_QUALITY, channels, nullptr);
    }

    void CaptureVideo::audioResamplerClear() {
        if (_audio_resampler) {
            src_delete(_audio_resampler);
            _audio_resampler = nullptr;
        }
    }

    void CaptureVideo::audioOnBufferGet(BYTE *ppDatas, UINT32 NumFramesRequested) {
        if (_capture_active.test()) {
            _audio_buffer = reinterpret_cast<float *>(ppDatas);
        }
    }

    void CaptureVideo::audioOnBufferRelease(UINT32 framesWritten) {

        if (!_capture_active.test()) {
            return;
        }

        if (!AudioSupported()) {
            return;
        }

        if (TimestampInited() && _audio_frequency) {
            // get time
            auto time_current = TimestampGet();

            // create MF buffer
            auto decimation = m_audio_core->WaveFormatFrequency() / _audio_frequency;
            auto mf_buf = MF::BufferCreateMemory(framesWritten * decimation * m_audio_core->WaveFormatChannels() * sizeof(float));
            if (!mf_buf) {
                CaptureStop();
                return;
            }

            // lock buffer
            uint8_t *out_data_u8 = nullptr;
            size_t out_channels = _audio_channels;
            if (FAILED(mf_buf->Lock(reinterpret_cast<uint8_t **>(&out_data_u8), nullptr, nullptr))) {
                CaptureStop();
                return;
            }
            int16_t *out_data_i16 = reinterpret_cast<int16_t *>(out_data_u8);
            float *out_data_f32 = reinterpret_cast<float *>(out_data_u8);


            // resampling
            float *in_data_f32 = reinterpret_cast<float *>(_audio_buffer);
            if (_audio_resampler) {
                SRC_DATA data{
                    .data_in = in_data_f32,
                    .data_out = out_data_f32,
                    .input_frames = (long)framesWritten,
                    .output_frames = (long)framesWritten * (long)decimation,
                    .end_of_input = 0U,
                    .src_ratio = (double)_audio_frequency / m_audio_core->WaveFormatFrequency(),
                };
                src_process(_audio_resampler, &data);
                framesWritten = data.output_frames_gen;
            } else {
                memcpy(out_data_f32, in_data_f32, framesWritten * m_audio_core->WaveFormatChannels() * sizeof(float));
            }

            // float->int16 conversion
            size_t in_channels = m_audio_core->WaveFormatChannels();
            for (size_t frame = 0; frame < framesWritten; frame++) {
                for (size_t channel = 0; channel < std::min(in_channels, out_channels); channel++) {
                    out_data_i16[frame * out_channels + channel] =
                        static_cast<int16_t>(out_data_f32[frame * in_channels + channel] * 32767.0);
                }
            }

            // unlock buffer
            if (FAILED(mf_buf->Unlock())) {
                CaptureStop();
                return;
            }

            // updater buffer size
            if (FAILED(mf_buf->SetCurrentLength(framesWritten * _audio_channels * sizeof(int16_t)))) {
                CaptureStop();
                return;
            }

            // create sample
            auto sample = MF::SampleCreate(mf_buf, time_current, framesWritten * 1'000'000'000 / _audio_frequency);
            if (!sample) {
                CaptureStop();
                return;
            }

            // write sample
            if (!m_mf_writer->AddSample(sample, m_mf_writer->GetStreamAudioIndex())) {
                CaptureStop();
                return;
            }
        }
    }



    //
    // Video/Image
    //

    void CaptureVideo::imageStart(uint32_t fps) {
        imageStop();
        imageTextureCreate();

        _image_frametime_ns = 1'000'000'000 / fps;
    }

    void WoT::Video::CaptureVideo::imageStop() {
        for (auto &texture : _image_texture) {
            texture.Reset();
        }

        _image_texture_next = 0U;
        _image_frametime_ns = 0U;
        _image_time_last = 0U;
    }

    void CaptureVideo::imageProcess() {
        if (!_capture_active.test()) {
            return;
        }

        int64_t time_current = TimestampGet();
        bool first_frame = false;
        if (!TimestampInited() || !_image_time_last || (time_current - _image_time_last) > (_image_frametime_ns / _image_oversampling)) {
            // update time
            if (!TimestampInited()) {
                timestampInit();
                first_frame = true;
            }
            _image_time_last = time_current;

            // get texture
            auto &texture = _image_texture[_image_texture_next];
            _image_texture_next = (_image_texture_next + 1) % _image_texture.size();

            // get backbuffer
            auto backbuffer = m_d3d->SwapchainGetBackbuffer();
            if (!backbuffer) {
                CaptureStop();
                return;
            }

            // copy backbuffer to texture
            if (!m_d3d->ResourceCopy(texture.Get(), backbuffer.Get())) {
                CaptureStop();
                return;
            }

            // create buffer
            auto mf_buffer = MF::BufferCreateTexture(texture);
            if (!mf_buffer) {
                CaptureStop();
                return;
            }

            // create sample
            auto sample = MF::SampleCreate(mf_buffer, time_current, 0ULL);
            if (!sample) {
                CaptureStop();
                return;
            }

            // set first discontiunity on first frame
            if (first_frame) {
                if (FAILED(sample->SetUINT32(MFSampleExtension_Discontinuity, TRUE))) {
                    CaptureStop();
                    return;
                }
            }

            // write sample
            if (!m_mf_writer->AddSample(sample, m_mf_writer->GetStreamVideoIndex())) {
                CaptureStop();
                return;
            }
        }
    }

    void CaptureVideo::imageTextureCreate() {
        for (auto &texture : _image_texture) {
            texture.Reset();
        }
        for (auto &texture : _image_texture) {
            texture =m_d3d->Texture2DCreate(m_d3d->SwapchainGetWidth(), m_d3d->SwapchainGetHeight(), m_d3d->SwapchainGetFormat(), 0U, false, false);
        }
        _image_texture_next = 0U;
    }

    void CaptureVideo::imageInputResize() {
        if (CaptureActive()) {
            imageTextureCreate();

            auto media_in = MF::MediaTypeGetVideoIn(m_d3d->SwapchainGetWidth(), m_d3d->SwapchainGetHeight());
            if (!media_in) {
                CaptureStop();
                return;
            }

            if (!m_mf_writer->SetMediaTypeInput(m_mf_writer->GetStreamVideoIndex(), media_in)) {
                CaptureStop();
                return;
            }
        }
    }



    //
    // Timestamp
    //

    bool CaptureVideo::TimestampInited() const { return _timestamp_val != 0LL; }

    int64_t CaptureVideo::TimestampGet() const {
        if (!_timestamp_val) {
            return 0LL;
        }
        return timestampSystemGet() - _timestamp_val;
    }

    void CaptureVideo::timestampInit() { _timestamp_val = timestampSystemGet(); }


    void CaptureVideo::timestampClear() { _timestamp_val = 0LL; }

    int64_t CaptureVideo::timestampSystemGet() const { return std::chrono::steady_clock::now().time_since_epoch().count(); }



    //
    // Hooks/CoreAudio
    //

    void CaptureVideo::hooksCoreaudioInit() {
        _hooks_coreaudio.init();
        if (_hooks_coreaudio.inited()) {
            _hooks_coreaudio.IAudioRenderClient_GetBuffer_register(
                "wot_video_video_capture", XFW::Native::Hooks::HookPlace::After,
                std::bind(&CaptureVideo::onAudioRenderClientGetBuffer, this, _1, _2, _3));
            _hooks_coreaudio.IAudioRenderClient_ReleaseBuffer_register(
                "wot_video_video_capture", XFW::Native::Hooks::HookPlace::After,
                std::bind(&CaptureVideo::onAudioRenderClientReleaseBuffer, this, _1, _2, _3));
        }
    }

    void CaptureVideo::hooksCoreaudioDeinit() {
        if (_hooks_coreaudio.inited()) {
            _hooks_coreaudio.IAudioRenderClient_GetBuffer_unregister("wot_video_video_capture", XFW::Native::Hooks::HookPlace::After);
            _hooks_coreaudio.IAudioRenderClient_ReleaseBuffer_unregister("wot_video_video_capture",
                                                                         XFW::Native::Hooks::HookPlace::After);
        }
    }

    void CaptureVideo::onAudioRenderClientGetBuffer(IAudioRenderClient *pAudioRenderClient, uint32_t NumFramesRequested,
                                                    uint8_t **ppDatas) {
        if (ppDatas) {
            audioOnBufferGet(*ppDatas, NumFramesRequested);
        }
    }

    void CaptureVideo::onAudioRenderClientReleaseBuffer(IAudioRenderClient *pAudioRenderClient, uint32_t NumFramesWritten, DWORD) {
        audioOnBufferRelease(NumFramesWritten);
    }



    //
    // Hooks/D3D
    //

    void CaptureVideo::hooksD3DInit() {
        if (_hooks_d3d.inited()) {
            _hooks_d3d.IDXGISwapChain_Present_register("wot_video_capture", XFW::Native::Hooks::HookPlace::Before,
                                                       std::bind(&CaptureVideo::onDxgiSwapchainPresent, this, _1, _2, _3));
            _hooks_d3d.IDXGISwapChain_ResizeBuffers_register(
                "wot_video_capture", XFW::Native::Hooks::HookPlace::After,
                std::bind(&CaptureVideo::onDxgiSwapchainResizeBuffers, this, _1, _2, _3, _4, _5, _6));
        }
    }

    void CaptureVideo::hooksD3DDeinit() {
        if (_hooks_d3d.inited()) {
            _hooks_d3d.IDXGISwapChain_Present_unregister("wot_video_capture", XFW::Native::Hooks::HookPlace::Before);
            _hooks_d3d.IDXGISwapChain_ResizeBuffers_unregister("wot_video_capture", XFW::Native::Hooks::HookPlace::After);
        }
    }

    void CaptureVideo::onDxgiSwapchainPresent(IDXGISwapChain *pSwapChain, UINT, UINT) {
        if (pSwapChain != _hooks_swapchain) {
            _hooks_swapchain = pSwapChain;
        }

        imageProcess();
    }

    void CaptureVideo::onDxgiSwapchainResizeBuffers(IDXGISwapChain *pSwapChain, UINT, UINT, UINT, DXGI_FORMAT, UINT) {
        if (pSwapChain != _hooks_swapchain) {
            _hooks_swapchain = pSwapChain;
        }

        imageInputResize();
    }

} // namespace WoT::Video
