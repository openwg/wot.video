CPMAddPackage(
  NAME stb
  GIT_REPOSITORY https://github.com/nothings/stb.git
  GIT_TAG af1a5bc352164740c1cc1354942b1c6b72eacb8a
  DOWNLOAD_ONLY YES
)

# littlefs has no CMake support, so we create our own target
if (stb_ADDED)
  add_library(stb INTERFACE)
  target_include_directories(stb INTERFACE $<BUILD_INTERFACE:${stb_SOURCE_DIR}>)
endif()
