// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Mikhail Paulyshka

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION

// stdlib
#include <fstream>
#include <string>

// wot.video
#include "wot_video_stb.h"

namespace WoT::Video {
    void stbi_write_ofstream(void *context, void *data, int size) {
        auto *file = reinterpret_cast<std::ofstream *>(context);
        if (!file || !file->is_open()) {
            return;
        }
        file->write(static_cast<char *>(data), size);
    }
} // namespace WoT::Video