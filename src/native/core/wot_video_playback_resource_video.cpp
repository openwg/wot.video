// SPDX-License-Identifier: MIT
// Copyright (c) 2016, 2023 Microsoft Corporation
// Copyright (c) 2025 Mikhail Paulyshka

// See also:
//          https://learn.microsoft.com/en-us/gaming/gdk/_content/gc/system/overviews/mediafoundation-decode
//          https://github.com/microsoft/Windows-universal-samples/blob/main/Samples/HolographicFaceTracking/cpp/Content/NV12VideoTexture.cpp
//          https://stackoverflow.com/a/31376104

#pragma once

//
// Includes
//

// windows
#include <mmdeviceapi.h>
#include <mfapi.h>

// STB
#include "stb_image.h"

// WoT.Video
#include "wot_video_logger.h"
#include "wot_video_playback_resource_video.h"
#include "wot_video_utils.h"



//
// Implementation
//

namespace WoT::Video {

    //
    // Init
    //

    PlaybackResourceVideo::~PlaybackResourceVideo() {
        threadDeinit();
    }

    bool PlaybackResourceVideo::Init(const std::wstring &path, IMFDXGIDeviceManager *dev_mgr) {
        if (!m_source.Init(path, dev_mgr)) {
            wot_video_logger::log("PlaybackResourceVideo::Init -> failed to Init");
            return false;
        }

        // media type
        Microsoft::WRL::ComPtr<IMFMediaType> media_type;
        if (FAILED(MFCreateMediaType(media_type.GetAddressOf()))) {
            wot_video_logger::log("PlaybackResourceVideo::Init -> failed to create media type");
            return false;
        }
        if (FAILED(media_type->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Video))) {
            wot_video_logger::log("PlaybackResourceVideo::Init -> failed to set major type");
            return false;
        }
        if (FAILED(media_type->SetGUID(MF_MT_SUBTYPE, MFVideoFormat_NV12))) {
            wot_video_logger::log("PlaybackResourceVideo::Init -> failed to set subtype");
            return false;
        }
        if (!m_source.SetCurrentMediaType(m_source.GetStreamVideoIndex(), media_type)) {
            wot_video_logger::log("PlaybackResourceVideo::Init -> failed to SetCurrentMediaType");
            return false;
        }

        // video size
        // TODO: use MF_MT_MINIMUM_DISPLAY_APERTURE
        // https://learn.microsoft.com/en-us/gaming/gdk/_content/gc/system/overviews/mediafoundation-decode
        std::tie(m_size_width, m_size_height) = m_source.GetVideoDimensions();
        if (!m_size_width || !m_size_height) {
            wot_video_logger::log("PlaybackResourceVideo::Init -> failed to GetVideoDimensions");
            return false;
        }
        m_rs_texture_width = m_size_width;
        m_rs_texture_height = m_size_height;

        // texture
        m_rs_texture = m_render_d3d->Texture2DCreate(m_rs_texture_width, m_rs_texture_height, DXGI_FORMAT_NV12, D3D11_BIND_SHADER_RESOURCE, false, false);
        if (!m_rs_texture) {
            wot_video_logger::log("PlaybackResourceVideo::Init -> failed to Texture2DCreate");
            return false;
        }

        // SRV
        auto srv_luma = std::move(m_render_d3d->SRVCreate(m_rs_texture, DXGI_FORMAT_R8_UNORM));
        if (!srv_luma) {
            wot_video_logger::log("PlaybackResourceVideo::Init -> failed to SRVCreate(LUMA)");
            return false;
        }

        auto srv_chroma = std::move(m_render_d3d->SRVCreate(m_rs_texture, DXGI_FORMAT_R8G8_UNORM));
        if (!srv_chroma) {
            wot_video_logger::log("PlaybackResourceVideo::Init -> failed to SRVCreate(CHROMA)");
            return false;
        }
        m_rs_srv.push_back(std::move(srv_luma));
        m_rs_srv.push_back(std::move(srv_chroma));

        if (!audioInit()) {
            return false;
        }

        m_started = true;
        m_timestamp_start = std::chrono::steady_clock::now();
        threadInit();

        wot_video_logger::log("PlaybackResourceVideo::Init -> OK");
        return true;
    }


    //
    // Audio
    //

    bool PlaybackResourceVideo::audioInit() {
        // request PCM
        {
            Microsoft::WRL::ComPtr<IMFMediaType> media_type;
            if (FAILED(MFCreateMediaType(media_type.GetAddressOf()))) {
                wot_video_logger::log("PlaybackResourceVideo::audioInit -> failed to create media type");
                return false;
            }
            if (FAILED(media_type->SetGUID(MF_MT_MAJOR_TYPE, MFMediaType_Audio))) {
                wot_video_logger::log("PlaybackResourceVideo::audioInit -> failed to set major type");
                return false;
            }
            if (FAILED(media_type->SetGUID(MF_MT_SUBTYPE, MFAudioFormat_PCM))) {
                wot_video_logger::log("PlaybackResourceVideo::audioInit -> failed to set subtype");
                return false;
            }
            if (FAILED(media_type->SetUINT32(MF_MT_AUDIO_NUM_CHANNELS, m_audio_core->WaveFormatChannels()))) {
                wot_video_logger::log("PlaybackResourceVideo::audioInit -> failed to set numchannels");
                return false;
            }
            if (FAILED(media_type->SetUINT32(MF_MT_AUDIO_BITS_PER_SAMPLE, m_audio_core->WaveFormatBitsPerSample()))) {
                wot_video_logger::log("PlaybackResourceVideo::audioInit -> failed to set bits per sample");
                return false;
            }
            if (FAILED(media_type->SetUINT32(MF_MT_AUDIO_SAMPLES_PER_SECOND, m_audio_core->WaveFormatFrequency()))) {
                wot_video_logger::log("PlaybackResourceVideo::audioInit -> failed to set bits per sample");
                return false;
            }
            if (!m_source.SetCurrentMediaType(m_source.GetStreamAudioIndex(), media_type)) {
                wot_video_logger::log("PlaybackResourceVideo::audioInit -> failed to SetCurrentMediaType");
                return false;
            }
        }

        // read current type
        UINT32 formatSize{};
        {
            Microsoft::WRL::ComPtr<IMFMediaType> media_type = m_source.GetStreamCurrentMediaType(m_source.GetStreamAudioIndex());
            if (!media_type) {
                wot_video_logger::log("PlaybackResourceVideo::audioInit -> failed to GetStreamCurrentMediaType");
                return false;
            }

            if (FAILED(MFCreateWaveFormatExFromMFMediaType(media_type.Get(), &m_audio_waveformat, &formatSize))) {
                wot_video_logger::log("PlaybackResourceVideo::audioInit -> failed to MFCreateWaveFormatExFromMFMediaType");
                return false;
            }
        }

        // get MMDevice
        auto *mm_device = m_audio_core->MMDeviceGet();
        if (!mm_device) {
            wot_video_logger::log("PlaybackResourceVideo::audioInit -> failed to MMDeviceGet");
            return false;
        }

        // activate IAudioClient
        if (FAILED(mm_device->Activate(__uuidof(IAudioClient), CLSCTX_ALL, nullptr, reinterpret_cast<void **>(m_audio_client.GetAddressOf())))) {
            wot_video_logger::log("PlaybackResourceVideo::audioInit -> failed to Activate");
            return false;
        }

        auto hr = m_audio_client->Initialize(AUDCLNT_SHAREMODE_SHARED, AUDCLNT_STREAMFLAGS_EVENTCALLBACK, 0, 0, m_audio_waveformat, nullptr);
        if (FAILED(hr)) {
            wot_video_logger::log("PlaybackResourceVideo::audioInit -> failed to Initialize", hr);
            return false;
        }

        // Initialize in shared mode with event-driven buffer
        m_audio_client_event = CreateEvent(nullptr, FALSE, FALSE, nullptr);
        if (FAILED(m_audio_client->SetEventHandle(m_audio_client_event))) {
            wot_video_logger::log("PlaybackResourceVideo::audioInit -> failed to SetEventHandle");
            return false;
        }

        if (FAILED(m_audio_client->GetService(__uuidof(IAudioRenderClient), reinterpret_cast<void **>(m_audio_client_render.GetAddressOf())))) {
            wot_video_logger::log("PlaybackResourceVideo::audioInit -> failed to SetEventHandle");
            return false;
        }

        if (FAILED(m_audio_client->GetBufferSize(&m_audio_buf_framecnt))) {
            wot_video_logger::log("PlaybackResourceVideo::audioInit -> failed to GetBufferSize");
            return false;
        }

        if (FAILED(m_audio_client->Start())) {
            wot_video_logger::log("PlaybackResourceVideo::audioInit -> failed to Start");
            return false;
        }

        return true;
    }

    void PlaybackResourceVideo::audioThreadProc() {
        if (!m_audio_finished || !m_audio_buf.empty()) {
            UINT32 padding{};
            m_audio_client->GetCurrentPadding(&padding);
            UINT32 availableFrames = m_audio_buf_framecnt - padding;
            UINT32 bytesNeeded = availableFrames * m_audio_waveformat->nBlockAlign;

            while (bytesNeeded > 0 && (!m_audio_buf.empty() || !m_audio_finished)) {
                if (m_audio_buf.empty() && !m_audio_finished) {
                    auto sample = m_source.GetSample(m_source.GetStreamAudioIndex());
                    if (sample->flags & MF_SOURCE_READERF_ENDOFSTREAM) {
                        m_audio_finished = true;
                        break;
                    }

                    Microsoft::WRL::ComPtr<IMFMediaBuffer> mf_buffer;
                    sample->sample->ConvertToContiguousBuffer(mf_buffer.GetAddressOf());

                    BYTE* pAudioData{};
                    DWORD dataLength{};
                    mf_buffer->Lock(&pAudioData, nullptr, &dataLength);
                    m_audio_buf.insert(m_audio_buf.end(), pAudioData, pAudioData + dataLength);
                    mf_buffer->Unlock();
                }

                size_t bytesToWrite = std::min<size_t>(bytesNeeded, m_audio_buf.size());
                if (bytesToWrite == 0) {
                    break;
                }

                BYTE* pDest{};
                UINT32 framesToWrite = bytesToWrite / m_audio_waveformat->nBlockAlign;
                m_audio_client_render->GetBuffer(framesToWrite, &pDest);
                memcpy(pDest, m_audio_buf.data(), bytesToWrite);
                m_audio_client_render->ReleaseBuffer(framesToWrite, 0);
                m_audio_buf.erase(m_audio_buf.begin(), m_audio_buf.begin() + bytesToWrite);
                bytesNeeded -= bytesToWrite;
            }
        }
    }



    //
    // Mask
    //

    bool PlaybackResourceVideo::MaskSet(const std::wstring &path) {
        m_mask_texture.Reset();

        if (path.empty()) {
            return false;
        }

        FILE *file = _wfopen(path.c_str(), L"rb");
        if (!file) {
            return false;
        }

        // Load the image using stbi_load_from_file
        int channels{};
        int mask_width{};
        int mask_height{};
        unsigned char *imageData = stbi_load_from_file(file, &mask_width, &mask_height, &channels, 1);
        if (!imageData || mask_width <= 0 || mask_height <= 0) {
            fclose(file);
            return false;
        }
        fclose(file);

        if (mask_width != m_rs_texture_width || mask_height != m_rs_texture_height) {
            wot_video_logger::log("PlaybackResourceVideo::MaskSet -> mask does not match the video size");
            return false;
        }

        m_mask_texture =
            m_render_d3d->Texture2DCreate(m_rs_texture_width, m_rs_texture_height, DXGI_FORMAT_R8_UNORM, D3D11_BIND_SHADER_RESOURCE, false, false, imageData);
        if (!m_mask_texture) {
            wot_video_logger::log("PlaybackResourceVideo::MaskSet -> failed to Texture2DCreate");
            stbi_image_free(imageData);
            return false;
        }

        stbi_image_free(imageData);

        m_mask_srv = m_render_d3d->SRVCreate(m_mask_texture);
        if (!m_mask_srv) {
            wot_video_logger::log("PlaybackResourceVideo::MaskSet -> failed to SRVCreate");
            return false;
        }

        return true;
    }



    //
    // Render
    //


    void PlaybackResourceVideo::RenderProcess() {
        std::scoped_lock lock(m_video_sample_mutex);

        if (m_video_finished) {
            return;
        }

        if (!m_video_sample_cur) {
            return;
        }

        if (!m_video_sample_cur->sample) {
            return;
        }

        Microsoft::WRL::ComPtr<IMFMediaBuffer> mf_buffer;
        if (FAILED(m_video_sample_cur->sample->GetBufferByIndex(0, mf_buffer.GetAddressOf()))) {
            wot_video_logger::log("PlaybackResourceVideo::processCopy -> failed to GetBufferByIndex");
            return;
        }

        Microsoft::WRL::ComPtr<IMFDXGIBuffer> mf_buffer_dxgi;
        if (FAILED(mf_buffer.As(&mf_buffer_dxgi))) {
            wot_video_logger::log("PlaybackResourceVideo::processCopy -> failed to cast to IMFDXGIBuffer");
            return;
        }

        Microsoft::WRL::ComPtr<ID3D11Texture2D> mf_texture;
        UINT mf_texture_idx{};
        if (FAILED(mf_buffer_dxgi->GetResource(IID_PPV_ARGS(&mf_texture)))) {
            wot_video_logger::log("PlaybackResourceVideo::processCopy -> failed to GetResource");
            return;
        }

        if (FAILED(mf_buffer_dxgi->GetSubresourceIndex(&mf_texture_idx))) {
            wot_video_logger::log("PlaybackResourceVideo::processCopy -> failed to GetSubresourceIndex");
            return;
        }

        D3D11_BOX SrcBox;
        SrcBox.left = 0;
        SrcBox.top = 0;
        SrcBox.front = 0;
        SrcBox.right = m_rs_texture_width;
        SrcBox.bottom = m_rs_texture_height;
        SrcBox.back = 1;

        m_render_d3d->DeviceGetContext()->CopySubresourceRegion(m_rs_texture.Get(), 0, 0, 0, 0, mf_texture.Get(), mf_texture_idx, &SrcBox);
        m_video_sample_cur->sample.Reset();
    }


    //
    // RS
    //

    std::vector<ID3D11ShaderResourceView *> PlaybackResourceVideo::RsGetSRV() {
        auto result = PlaybackResource::RsGetSRV();
        if (m_mask_srv) {
            result.push_back(m_mask_srv.Get());
        }

        return result;
    }



    //
    // Thread
    //

    void PlaybackResourceVideo::threadInit() {
        threadDeinit();
        m_thread_finished = false;
        m_thread = std::thread(&PlaybackResourceVideo::threadProc, this);
        Utils::SetThreadName(m_thread, L"wot_video_playback");
    }

    void PlaybackResourceVideo::threadDeinit() {
        m_thread_finished = true;
        if (m_thread.joinable()) {
            m_thread.join();
        }
    }

    void PlaybackResourceVideo::threadProc() {
        while (!m_thread_finished) {
            auto wait_result = WaitForSingleObject(m_audio_client_event, m_thread_timeout);
            if (wait_result == WAIT_OBJECT_0) {
                audioThreadProc();
            }
            videoThreadProc();
        }
    }



    //
    // Video
    //

    bool PlaybackResourceVideo::videoThreadProc() {
        if (m_video_finished) {
            return true;
        }

        if (!m_video_sample_next) {
            wot_video_logger::log("PlaybackResourceVideo::videoThreadProc -> GetSample");
            m_video_sample_next = std::move(m_source.GetSample(m_source.GetStreamVideoIndex()));
        }

        {
            std::scoped_lock lock(m_video_sample_mutex);

            auto current_time = std::chrono::duration_cast<std::chrono::nanoseconds>((std::chrono::steady_clock::now() - m_timestamp_start));
            if (!m_video_sample_cur || std::chrono::nanoseconds(m_video_sample_cur->timestamp_end) < current_time) {
                wot_video_logger::log("PlaybackResourceVideo::videoThreadProc -> Promote");
                videoSamplePromote();
            }

            if (m_video_sample_cur && m_video_sample_cur->flags & MF_SOURCE_READERF_ENDOFSTREAM) {
                m_video_finished = true;
            }
        }

        return true;
    }

    bool PlaybackResourceVideo::videoSamplePromote() {
        m_video_sample_cur = std::move(m_video_sample_next);
        m_video_sample_next.reset();
        return true;
    }



    //
    // Visibility
    //

    bool PlaybackResourceVideo::VisibilityGet() const {
        if (!m_visibility) {
            return false;
        }
        if (!m_visibility_on_finish && m_video_finished) {
            return false;
        }
        return true;
    }

    bool PlaybackResourceVideo::VisibilityOnFinishSet(bool val) {
        m_visibility_on_finish = val;
        return true;
    }


} // namespace WoT::Video
