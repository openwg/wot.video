// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Mikhail Paulyshka

//
// Includes
//

// stdlib
#include <iostream>

// wot.video
#include "wot_video_mf.h"
#include "wot_video_video_editor.h"



//
// Functions
//

int main() {
    //
    // COM
    //
    if (FAILED(CoInitializeEx(nullptr, COINIT_MULTITHREADED))) {
        std::cerr << "Failed to initialize COM" << std::endl;
        return 1;
    }

    //
    // Settings
    //

    std::wstring folder = L"G:\\";
    auto file_main_in = L"m2-res_480p.mp4";
    auto file_main_out = L"m2-res_480p_out.mp4";
    auto fragments = std::vector<std::tuple<std::wstring, int64_t, int64_t>>{
        {L"m2-res_480p.mp4", 0 * 1'000'000'000, 2 * 1'000'000'000}
    };


    //
    // Process
    //

    // init editor
    WoT::Video::VideoEditor editor;
    if (!editor.Init({})) {
        std::cerr << "Failed to initialize Media Foundation" << std::endl;
        return 2;
    }

    // open files
    for (auto &[filename, time_in, time_out] : fragments) {
        auto filepath = folder + filename;
        if (editor.InputExists(filepath)) {
            continue;
        }
        if (!editor.InputOpen(filepath)) {
            std::cerr << "Failed to open input" << std::endl;
            return 4;
        }
    }

    // create output
    if (!editor.OutputCreate(folder + file_main_in, folder + file_main_out)) {
        std::cerr << "Failed to create output" << std::endl;
        return 5;
    }

    // process fragmens
    for (auto &[filename, time_in, time_out] : fragments) {
        if (!editor.OperationAddFragment(folder + filename, time_in, time_out)) {
            std::cerr << "Failed to add fragment" << std::endl;
        }
    }

    // close output
    if (!editor.OutputClose()) {
        std::cerr << "Failed to close output" << std::endl;
    }

    // close inputs
    if (!editor.InputCloseAll()) {
        std::cerr << "Failed to close inputs" << std::endl;
    }

    // deinit editor
    if (!editor.Deinit()) {
        std::cerr << "Failed to deinit" << std::endl;
    }

    // finish
    std::cout << "OK";
    return 0;
}
