"""
SPDX-License-Identifier: MIT
Copyright (c) 2023 Andrii Andrushchyshyn
Copyright (c) 2023 Mikhail Paulyshka
"""

#
# Imports
#

# cpython
import json
import os

# BigWorld
from debug_utils import LOG_CURRENT_EXCEPTION
from math_utils import clamp

#
# Helpers
#
from .utils import byteify, parse_integer

#
# Config
#

class VideoConfig:

	enabled = True
	width = 1920
	height = 1080
	fps = 30
	bitrate_audio = 96 * 1000
	bitrate_video = 16 * 1024 * 1024
	_filename_video = 'last_battle.mp4'

	@property
	def filepath_video(self):
		return '%s/replays/video/%s' % (os.getcwd(), self._filename_video)

	def __init__(self):
		settings_file = 'replays/video/config.json'
		settings_dir = os.path.dirname(settings_file)
		try:
			if not os.path.isdir(settings_dir):
				os.makedirs(settings_dir)
			if os.path.isfile(settings_file):
				with open(settings_file, 'rb') as fh:
					data = byteify(json.load(fh))
					self.enabled = data.get('enabled', True)
					self.width = parse_integer(data.get('width', 1920))
					self.height = parse_integer(data.get('height', 1280))
					self.fps = parse_integer(data.get('fps', 30))
					self.bitrate_audio = parse_integer(data.get('bitrate_audio', 96)) * 1000
					self.bitrate_video = parse_integer(data.get('bitrate_video', 16)) * 1024 * 1024
					self._filename_video = data.get('filename', 'last_battle.mp4')
			else:
				self.dump()
		except IOError:
			pass
		except:
			LOG_CURRENT_EXCEPTION()

		# clamp config values
		if self.width: self.width = clamp(1024, 3840, self.width)
		if self.height: self.height = clamp(720, 3840, self.height)
		if self.fps: self.fps = clamp(10, 240, self.fps)
		if self.bitrate_audio: self.bitrate_audio = clamp(96000, 192000, self.bitrate_audio)
		if self.bitrate_video: self.bitrate_video = clamp(5000000, 75000000, self.bitrate_video)

	def __str__(self):
		return '<VideoConfig width=%s height=%s fps=%s bitrate_audio=%s bitrate_video=%s file_video=%s>' % ( \
			self.width, self.height, self.fps, self.bitrate_audio, self.bitrate_video, self.filepath_video )

	def pack(self):
		return {
			'enabled': self.enabled,
			'width': self.width,
			'height': self.height,
			'fps': self.fps,
			'bitrate_audio': self.bitrate_audio / 1000,
			'bitrate_video': self.bitrate_video / (1024 * 1024),
			'filename': self._filename_video
		}

	def dump(self):
		settings_file = 'replays/video/config.json'
		settings_dir = os.path.dirname(settings_file)
		if not os.path.isdir(settings_dir):
			os.makedirs(settings_dir)
		with open(settings_file, 'wb') as fh:
			json.dump(self.pack(), fh)
