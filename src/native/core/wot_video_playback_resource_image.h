#pragma once

//
// Includes
//

// stdlib
#include <string>

// WoT.Video
#include "wot_video_playback_resource.h"



//
// Class
//

namespace WoT::Video {
    class PlaybackResourceImage: public PlaybackResource {
    public:
        PlaybackResourceImage(std::shared_ptr<D3D> d3d) : PlaybackResource(d3d) {}
        ~PlaybackResourceImage() override = default;

        bool Init(const std::wstring &path);

        //RS
    private:
        Microsoft::WRL::ComPtr<ID3D11Texture2D> m_rs_texture;
    };
}
