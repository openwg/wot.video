//
// Includes
//

// windows
#include <mfapi.h>
#include <mfidl.h>

// WoT.Video
#include "wot_video_mf_sink.h"





//
// Implementation
//

namespace WoT::Video {

    bool VideoSink::Init(const std::wstring &filename, IMFDXGIDeviceManager *dev_mgr, Microsoft::WRL::ComPtr<IMFMediaType> &aud_in,
                         Microsoft::WRL::ComPtr<IMFMediaType> &aud_out, Microsoft::WRL::ComPtr<IMFMediaType> &vid_in,
                         Microsoft::WRL::ComPtr<IMFMediaType> &vid_out) {

        // set attributes
        Microsoft::WRL::ComPtr<IMFAttributes> attributes;
        if (FAILED(MFCreateAttributes(attributes.GetAddressOf(), 5U))) {
            return false;
        }
        if (FAILED(attributes->SetUINT32(MF_LOW_LATENCY, TRUE))) {
            return false;
        }
        if (FAILED(attributes->SetUINT32(MF_SINK_WRITER_DISABLE_THROTTLING, TRUE))) {
            return false;
        }
        if (FAILED(attributes->SetGUID(MF_TRANSCODE_CONTAINERTYPE, MFTranscodeContainerType_MPEG4))) {
            return false;
        }

        // pass device manager
        if (dev_mgr) {
            if (FAILED(attributes->SetUINT32(MF_READWRITE_ENABLE_HARDWARE_TRANSFORMS, TRUE))) {
                return false;
            }
            if (FAILED(attributes->SetUnknown(MF_SINK_WRITER_D3D_MANAGER, dev_mgr))) {
                return false;
            }
        }


        // create writer
        if (FAILED(MFCreateSinkWriterFromURL(filename.c_str(), nullptr, attributes.Get(), m_writer.GetAddressOf()))) {
            return {};
        }

        // set video type
        if (vid_out) {
            DWORD idx_vid_out{};
            if (FAILED(m_writer->AddStream(vid_out.Get(), &idx_vid_out))) {
                return false;
            }

            if (vid_in && !SetMediaTypeInput(idx_vid_out, vid_in)) {
                return false;
            }
        }

        // set audio type
        if (aud_out) {
            DWORD idx_aud_out{};
            if (FAILED(m_writer->AddStream(aud_out.Get(), &idx_aud_out))) {
                return false;
            }

            if (aud_in && !SetMediaTypeInput(idx_aud_out, aud_in)) {
                return false;
            }
        }

        return true;
    }


    bool VideoSink::AddSample(Microsoft::WRL::ComPtr<IMFSample> &sample, uint32_t stream_index) {
        bool result = false;

        if (m_writer && sample && stream_index != std::numeric_limits<uint32_t>::max()) {
            result = SUCCEEDED(m_writer->WriteSample(stream_index, sample.Get()));
        }

        return result;
    }


    Microsoft::WRL::ComPtr<IMFMediaType> VideoSink::GetMediaTypeOutput(uint32_t stream_index) {
        if (m_writer) {
            Microsoft::WRL::ComPtr<IMFMediaSink> sink_media{};
            if (SUCCEEDED(m_writer->GetServiceForStream(MF_SINK_WRITER_MEDIASINK, GUID_NULL, IID_PPV_ARGS(&sink_media)))) {
                Microsoft::WRL::ComPtr<IMFStreamSink> sink_stream{};
                if (SUCCEEDED(sink_media->GetStreamSinkByIndex(stream_index, sink_stream.GetAddressOf()))) {
                    Microsoft::WRL::ComPtr<IMFMediaTypeHandler> media_type_handler{};
                    if (SUCCEEDED(sink_stream->GetMediaTypeHandler(media_type_handler.GetAddressOf()))) {
                        Microsoft::WRL::ComPtr<IMFMediaType> media_type{};
                        if (SUCCEEDED(media_type_handler->GetMediaTypeByIndex(0U, media_type.GetAddressOf()))) {
                            return media_type;
                        }
                    }
                }
            }
        }

        return {};
    }


    uint32_t VideoSink::GetStreamAudioIndex() const {
        return m_audio_idx;
    }


    uint32_t VideoSink::GetStreamVideoIndex() const { return m_video_idx; }


    uint32_t VideoSink::GetStreamIndex(GUID major_type) {
        uint32_t result = std::numeric_limits<uint32_t>::max();
        if (m_writer) {

            Microsoft::WRL::ComPtr<IMFMediaSink> sink_media{};
            if (SUCCEEDED(m_writer->GetServiceForStream(MF_SINK_WRITER_MEDIASINK, GUID_NULL, IID_PPV_ARGS(&sink_media)))) {

                DWORD sink_count = 0;
                sink_media->GetStreamSinkCount(&sink_count);
                for (uint32_t idx = 0; idx < sink_count; idx++) {

                    Microsoft::WRL::ComPtr<IMFStreamSink> sink_stream{};
                    if (SUCCEEDED(sink_media->GetStreamSinkByIndex(idx, sink_stream.GetAddressOf()))) {

                        Microsoft::WRL::ComPtr<IMFMediaTypeHandler> media_type_handler{};
                        if (SUCCEEDED(sink_stream->GetMediaTypeHandler(media_type_handler.GetAddressOf()))) {

                            GUID stream_type{};
                            if (SUCCEEDED(media_type_handler->GetMajorType(&stream_type))) {
                                if (stream_type == major_type) {
                                    result = idx;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        return result;
    }


    bool VideoSink::NotifyEOS(uint32_t stream_index) {
        bool result = false;

        if (m_writer) {
            result = SUCCEEDED(m_writer->NotifyEndOfSegment(stream_index));
        }

        return result;
    }


    bool VideoSink::SetMediaTypeInput(uint32_t stream_index, Microsoft::WRL::ComPtr<IMFMediaType> &media_type) {
        bool result = false;

        if (media_type && stream_index != std::numeric_limits<uint32_t>::max()) {
            auto res = m_writer->SetInputMediaType(stream_index, media_type.Get(), nullptr);
            result = SUCCEEDED(res);
        }

        return result;
    }


    bool VideoSink::SendTick(uint32_t stream_index, uint64_t timestamp_ns) {
        bool result = false;

        if (m_writer) {
            result = SUCCEEDED(m_writer->SendStreamTick(stream_index, timestamp_ns / 100ULL));
        }

        return result;
    }


    bool VideoSink::Start() {
        if (!m_writer) {
            return false;
        }

        if (FAILED(m_writer->BeginWriting())){
            return false;
        }

        // remember stream indexes
        m_audio_idx = GetStreamIndex(MFMediaType_Audio);
        m_video_idx = GetStreamIndex(MFMediaType_Video);
        return true;
    }


    bool VideoSink::Stop() {
        bool result = false;
        if (m_writer) {
            result = SUCCEEDED(m_writer->Finalize());
        }

        return result;
    }


    int64_t VideoSink::TimestampAudioGet() const {
        return m_timestamp_audio;
    }


    int64_t VideoSink::TimestampVideoGet() const {
        return m_timestamp_video;
    }


    void VideoSink::TimestampAudioSet(int64_t val) {
        m_timestamp_audio = val;
    }


    void VideoSink::TimestampVideoSet(int64_t val) {
        m_timestamp_video = val;
    }

} // namespace WoT::Video