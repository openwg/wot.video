// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Andrey Andruschyshyn
// Copyright (c) 2022 Mikhail Paulyshka

#pragma once

// stdlib
#include <array>
#include <atomic>
#include <memory>
#include <string>

// Windows
#include <Windows.h>
#include <wrl.h>

// Direct3D
#include <d3d11.h>
#include <dxgi.h>

// libsamplerate
#include "samplerate.h"

// wot.video
#include "wot_video_coreaudio.h"
#include "wot_video_d3d.h"
#include "wot_video_mf.h"

// xfw.native.hooks
#include "wot_video_mf_platform.h"
#include "wot_video_mf_sink.h"
#include "xfw_native_hooks_coreaudio.h"
#include "xfw_native_hooks_d3d.h"



namespace WoT::Video {
    class CaptureVideo {
        //
        // Init
        //
      public:
        CaptureVideo(XFW::Native::Hooks::HookmanagerD3D& hookmanager_d3d);
        ~CaptureVideo();
        bool Init(std::shared_ptr<CoreAudio> coreaudio, std::shared_ptr<D3D> d3d, std::shared_ptr<VideoPlatform> platform);
        bool Deinit();

      private:
        bool _inited = false;


        //
        // Components
        //
      private:
        std::shared_ptr<D3D> m_d3d;

        //
        // MF
        //
      private:
        void mfStop();
      private:
        std::shared_ptr<VideoPlatform> m_mf_platform;
        std::shared_ptr<VideoSink> m_mf_writer;;

        //
        // Video
        //
      public:
        bool CaptureActive() const;
        bool CaptureStart(const std::wstring &filename, uint32_t width, uint32_t height, uint32_t fps, uint32_t bitrate_audio,
                          uint32_t bitrate_video);
        bool CaptureStop();

      private:
        std::atomic_flag _capture_active{};


        //
        // Audio
        //
      public:
        bool AudioSupported() const;

      private:
        void audioStart();
        void audioStop();

        void audioResamplerInit(size_t channels);
        void audioResamplerClear();

        void audioOnBufferGet(BYTE *ppDatas, UINT32 NumFramesRequested);
        void audioOnBufferRelease(UINT32 framesWritten);


      private:
        std::shared_ptr<CoreAudio> m_audio_core;
        float *_audio_buffer = nullptr;
        size_t _audio_channels = 0U;
        size_t _audio_frequency = 0U;
        SRC_STATE *_audio_resampler = nullptr;


        //
        // Image
        //
      private:
        void imageProcess();
        void imageStart(uint32_t fps);
        void imageStop();
        void imageTextureCreate();
        void imageInputResize();

      private:
        static constexpr size_t _image_oversampling = 2U;
        static constexpr size_t _image_texture_count = 3U;

      private:
        uint64_t _image_frametime_ns = 0U;
        uint64_t _image_time_last = 0U;
        std::array<Microsoft::WRL::ComPtr<ID3D11Texture2D>, _image_texture_count> _image_texture;
        size_t _image_texture_next = 0U;


        //
        // Timestamp
        //
      public:
        [[nodiscard]] int64_t TimestampGet() const;
        [[nodiscard]] bool TimestampInited() const;

      private:
        void timestampInit();

        void timestampClear();
        [[nodiscard]] int64_t timestampSystemGet() const;

      private:
        int64_t _timestamp_val = 0LL;

        //
        // Hooks/CoreAudio
        //
      private:
        void hooksCoreaudioInit();
        void hooksCoreaudioDeinit();

        void onAudioClientInitialize(IAudioClient *pAudioClient, AUDCLNT_SHAREMODE, DWORD, REFERENCE_TIME, REFERENCE_TIME,
                                     const WAVEFORMATEX *, LPCGUID);
        void onAudioRenderClientGetBuffer(IAudioRenderClient *pAudioRenderClient, uint32_t NumFramesRequested, uint8_t **ppDatas);
        void onAudioRenderClientReleaseBuffer(IAudioRenderClient *pAudioRenderClient, uint32_t NumFramesWritten, DWORD);

      private:
        XFW::Native::Hooks::HookmanagerCoreaudio &_hooks_coreaudio;


        //
        // Hooks/Direct3D
        //
      private:
        void hooksD3DInit();
        void hooksD3DDeinit();

        void onDxgiSwapchainPresent(IDXGISwapChain *swapChain, UINT, UINT);
        void onDxgiSwapchainResizeBuffers(IDXGISwapChain *swapChain, UINT, UINT, UINT, DXGI_FORMAT, UINT);

      private:
        IDXGISwapChain* _hooks_swapchain{};
        XFW::Native::Hooks::HookmanagerD3D& _hooks_d3d;
    };
} // namespace WoT::Video
