#pragma once

//
// Includes
//

// Windows
#include <wrl.h>
#include <d3d11.h>

// WoT.Video
#include "wot_video_d3d.h"



//
// Class
//

namespace WoT::Video {
    class PlaybackResource {
        // Ctor
    protected:
        PlaybackResource(std::shared_ptr<D3D> d3d);
    public:
        virtual ~PlaybackResource() = default;

        // Position
    public:
        bool PositionSet(int x, int y);
    protected:
        int m_position_x{};
        int m_position_y{};

        // Mask
    public:
        virtual bool MaskSet(const std::wstring& path) {return false;}

        //Render
    public:
        virtual void RenderProcess() {} ;
    protected:
        std::shared_ptr<D3D> m_render_d3d;

        // RS
    public:
        virtual std::vector<ID3D11ShaderResourceView*> RsGetSRV();
        D3D11_VIEWPORT RsGetViewport();
    protected:
        std::vector<Microsoft::WRL::ComPtr<ID3D11ShaderResourceView>> m_rs_srv;

        // Size
    public:
        [[nodiscard]] std::pair<int, int> SizeGet() const;
        bool SizeSet(int width, int height);
    protected:
        int m_size_width{};
        int m_size_height{};

        // Visibility
    public:
        [[nodiscard]] virtual bool VisibilityGet() const;
        bool VisibilitySet(bool val);
        virtual bool VisibilityOnFinishSet(bool val);
    protected:
        bool m_visibility{false};
    };
}