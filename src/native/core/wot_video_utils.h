// SPDX-License-Identifier: MIT
// Copyright (c) 2025 Mikhail Paulyshka
#pragma once

//
// Includes
//

// stdlib
#include <string>
#include <string_view>
#include <thread>



//
// Class
//

namespace WoT::Video {
    namespace Utils {
        bool SetThreadName(std::thread& thread, std::wstring_view thread_name);
        void SetThreadName(DWORD thread_id, std::string_view thread_name);

        std::string ToString(std::wstring_view wstrv);
    }
}