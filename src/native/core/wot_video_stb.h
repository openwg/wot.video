// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Mikhail Paulyshka

// stb
#include <stb_image.h>
#include <stb_image_write.h>

namespace WoT::Video {
    void stbi_write_ofstream(void *context, void *data, int size);
} // namespace WoT::Video