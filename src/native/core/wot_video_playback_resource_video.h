#pragma once

//
// Includes
//

// stdlib
#include <string>

// WoT.Video
#include "wot_video_coreaudio.h"
#include "wot_video_mf_source.h"
#include "wot_video_playback_resource.h"
#include "wot_video_video_editor.h"



//
// Class
//

namespace WoT::Video {
    class PlaybackResourceVideo: public PlaybackResource {
    public:
        PlaybackResourceVideo(std::shared_ptr<CoreAudio> coreaudio, std::shared_ptr<D3D> d3d) : PlaybackResource(d3d), m_audio_core(std::move(coreaudio)) {}
        ~PlaybackResourceVideo() override;

        bool Init(const std::wstring &path, IMFDXGIDeviceManager* dev_mgr);

    private:
        VideoSource m_source;

        // Audio
    private:
        bool audioInit();
        void audioThreadProc();
    private:
        WAVEFORMATEX* m_audio_waveformat{};
        std::shared_ptr<CoreAudio> m_audio_core{};
        Microsoft::WRL::ComPtr<IAudioClient> m_audio_client{};
        Microsoft::WRL::ComPtr<IAudioRenderClient> m_audio_client_render{};
        HANDLE m_audio_client_event{};
        VideoSample m_audio_sample{};
        bool m_audio_finished{};
        std::vector<BYTE> m_audio_buf;
        uint32_t m_audio_buf_framecnt{};


        // Processing

    private:
        bool m_started{false};
        std::chrono::steady_clock::time_point m_timestamp_start{};



        // Mask
    public:
        bool MaskSet(const std::wstring& path) override;
    private:
        Microsoft::WRL::ComPtr<ID3D11Texture2D> m_mask_texture;
        Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> m_mask_srv;

        // Render
    public:
        void RenderProcess() override;

        //Texture
    public:
        std::vector<ID3D11ShaderResourceView*> RsGetSRV() override;
    private:
        Microsoft::WRL::ComPtr<ID3D11Texture2D> m_rs_texture;
        uint32_t m_rs_texture_width{};
        uint32_t m_rs_texture_height{};

        // Thread
    private:
        void threadInit();
        void threadDeinit();
        void threadProc();
    private:
        std::thread m_thread;
        bool m_thread_finished{};
        static constexpr auto m_thread_timeout{10};

        // Video
    private:
        bool videoThreadProc();
        bool videoSamplePromote();
    private:
        std::unique_ptr<VideoSample> m_video_sample_next{};
        std::unique_ptr<VideoSample> m_video_sample_cur{};
        std::mutex m_video_sample_mutex;
        bool m_video_finished{false};

        // Visibility
    public:
        [[nodiscard]] bool VisibilityGet() const override;
        bool VisibilityOnFinishSet(bool val) override;
    private:
        bool m_visibility_on_finish{false};
    };
}
