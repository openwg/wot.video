# OpenWG.WoT.Video

## Known Limitations

* capture/audio: audio for devices with samplerate lower than 44.1 will not be recorded
* capture/audio: 5.1 and 7.1 sound will be trimmed to 2.0
* capture/audio: may be in conflict with other audio sources (Vivox, XFW.Mumble, any in-game overlays with sound)

* capture/video: no video encoder profile setting
* capture/video: no HEVC option
* capture/video: software fallback might be broken
* capture/video: iGPU + dGPU scenario might be broken

* hooks: cannot deinit properly

* python: cannot deinit properly

