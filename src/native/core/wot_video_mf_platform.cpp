#pragma once

//
// Includes
//

// windows
#include <mfapi.h>

// WoT.Video
#include "wot_video_mf_platform.h"

#include "wot_video_logger.h"



//
// Class
//

namespace WoT::Video {
    VideoPlatform::VideoPlatform() {}

    VideoPlatform::~VideoPlatform() { Deinit(); }

    //
    // Init
    //

    bool VideoPlatform::Init(std::shared_ptr<D3D> d3d) {
        if (!d3d) {
            return false;
        }
        m_d3d = std::move(d3d);

        if (FAILED(MFStartup(MF_VERSION))) {
            return false;
        }

        m_mf_manager_token = std::numeric_limits<uint32_t>::max();
        if (FAILED(MFCreateDXGIDeviceManager(&m_mf_manager_token, m_mf_manager.GetAddressOf()))) {
            return false;
        }

        auto *device = m_d3d->DeviceGet();
        if (device) {
            MFDeviceSet(device);
        }
        m_d3d->HandlerDeviceRegister("wot_video_mf_platform", [this]() { MFDeviceSet(m_d3d->DeviceGet()); });

        return true;
    }

    void VideoPlatform::Deinit() {
        m_d3d->HandlerDeviceUnregister("wot_video_mf_platform");
        m_d3d.reset();
        m_mf_manager.Reset();
        MFShutdown();
    }


    //
    // MF
    //

    IMFDXGIDeviceManager *VideoPlatform::MFGetDeviceManager() { return m_mf_manager.Get(); }

    bool VideoPlatform::MFDeviceSet(ID3D11Device *device) {

        if (!device) {
            wot_video_logger::log("VideoPlatform::MFDeviceSet -> failed to get device ptr");
            return false;
        }

        if (!m_mf_manager) {
            wot_video_logger::log("VideoPlatform::MFDeviceSet -> failed get manager");
            return false;
        }

        if (m_mf_manager_token == std::numeric_limits<uint32_t>::max()) {
            wot_video_logger::log("VideoPlatform::MFDeviceSet -> failed to read token");
            return false;
        }

        if (FAILED(m_mf_manager->ResetDevice(device, m_mf_manager_token))) {
            wot_video_logger::log("VideoPlatform::MFDeviceSet -> failed to ResetDevice", device);
            return false;
        }

        wot_video_logger::log("VideoPlatform::MFDeviceSet -> OK, device", device);
        return true;
    }
} // namespace WoT::Video
