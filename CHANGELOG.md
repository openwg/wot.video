# Changelog

## v1.0.0 (2024.01.22)

* treewide: fix public version
* treewide: renamed from [redacted] to OpenWG.WoT.Video
* coreaudio: fix audio capture since World of Tanks 1.21.0

## v0.4.1 (2022.11.09)

* python: start recording after countdown
* video/editor: fix crash on audio segments with start offset higher than their duration

## v0.4.0 (2022.09.02)

* build: match with latest XFW.Utils changes
* python: [redacted]
* python: [redacted]
* python: fix vehicleID receiving
* python: improve capture start/stop algorithm
* python: perform automatic configuration dump
* video/capture: fix crash on devices with audio output frequency < 44100 Hz
* video/editor: fix crash on devices without audio output devices

## v0.3.1 (2022.08.10)

* build: XFW SDK changes
  
## v0.3.0 (2022.08.09)

* general: World of Tanks 1.18.0 support

## v0.2.0 (2022.07.24)

* demo: add ability to set several input files
* demo: add COM initialization
* demo: always try to finalize file
* mf: add ability to compare media types
* mf: add ability to produce derived media types
* mf: add ability to get SinkWriter output media type
* mf: cleanup MFSample copy routine
* mf: expose sample duration and end timestamp
* mf: fix DXGI device manager reset return value
* mf: fix SourceReader device manager binding
* video_editor: add ability to bind with DXGI device manager
* video_editor: add ability to check input for existence
* video_editor: do no try to process non-existent streams
* video_editor: refactor `FragmentAdd` operation
* video_editor: always use transcoding for audio stream

## v0.1.2 (2022.07.20)

* python: revamp of event recording
* python: optimization

## v0.1.1 (2022.07.11)

* core: fix compatibility with XFW.Native v2.7.0

## v0.1.0 (2022.07.07)

* mf: refactoring, more flexible access to SourceReader and SinkWriter
* python: initial event recorder
* python: initial trimming invocation
* video_capture: fix crash on disposing
* video_editor: add support for multiple sources
* video_editor: fix stream tick processing
* video_editor: add ability to insert fragment till the end of input file
* video_editor: fix video seeking

## v0.0.9 (2022.06.30)

* build: split python module to the separate library
* build: cleanup python bytecode compilation
* capture: mark first frame as discontiunity frame
* capture: send end of stream notification before finalization
* capture: improve frame pacing, do not setup frame duration for image
* mf: split SinkWriter initialization and writing
* mf: implement SourceReader support
* hooks: add ability to unsubscribe
* python: add ability to receive current timestamp
* python: cleanup, do not perform any actions on importing
* trim: initial implementation

## v0.0.8 (2022.06.19)

* capture: fix crash on non-available D3D device
* capture: cleanup, divide video and screenshot to different classes
* hooks: refactor
* python: make pathes more consistent with replays

## v0.0.7 (2022.06.13)

* capture: fix video capture for systems without output sound devices
* hooks: little refactor to allow subscription from class methods

## v0.0.6 (2022.06.11)

* capture: resample higher audio samplerates to 44.1/48 KHz
* capture: handle devices with >2 audio channels
* python: use events instead of overrides

## v0.0.5 (2022.06.06)

* capture: fix audio desync

## v0.0.4 (2022.06.06)

* capture: allow window resize without video stop
* capture: use PNG for screenshots
* capture: implemented sound capture
* coreaudio: new component
* d3d: refactor, extraction from capture
* hooks: refactor
* hooks/coreaudio: add support for IAudioClient/IAudioRenderClient hooking
* hooks/d3d: remove Kiero dependency
* mf: refactor, extraction from capture
* other: add clang-format
* python: capture screenshot on start

## v0.0.3 (2022.06.03)

* capture: add ability to set arbitrary width and height
* capture: do not create a new texture on each frame
* mf: cleanup
* mf: stabilize video framerate
* python: add basic battle recording with json configuration


## v0.0.2 (2022.06.01)
* capture/d3d: create textures without CPU access for video encoder
* capture/d3d: enable device multithread protection to prevent crash on NVIDIA cards
* capture/d3d: do not share texture between samples
* capture/mf: set input stream samples as independent
* capture/mf: disable throttling
* capture/mf: send sample to writer only in case of 1/2 frametime passing
* d3d/helpers: fix return values
* d3d/formats: cleanup


## v0.0.1 (2022.05.29)

* initial version
