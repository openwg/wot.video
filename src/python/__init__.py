"""
SPDX-License-Identifier: MIT
Copyright (c) 2024 Andrii Andrushchyshyn
Copyright (c) 2024 Mikhail Paulyshka
"""

#
# Imports
#

from xfw_native.python import XFWNativeModuleWrapper

#
# Globals
#

# public
g_video_config = None
g_video_native_module = None
g_video_native_client = None


# private
__wot_video_loaded = False



#
# XFW.Loader API
#

def xfw_module_init():
    global __wot_video_loaded
    global g_video_config
    global g_video_native_module
    global g_video_native_client

    from .config import VideoConfig
    g_video_config = VideoConfig()

    g_video_native_module = XFWNativeModuleWrapper("openwg.wot.video","wot_video.pyd", "WoT_Video")
    g_video_native_client = g_video_native_module.WoT_Video_Client()
    g_video_native_client.init()

    from .hooks import hooks_init
    hooks_init()

    __wot_video_loaded = True

def xfw_is_module_loaded():
    global __wot_video_loaded
    return __wot_video_loaded
