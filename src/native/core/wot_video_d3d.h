// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Mikhail Paulyshka

#pragma once

// stdlib
#include <cstdint>
#include <vector>

// Windows
#include <Windows.h>
#include <wrl.h>

// Direct3D
#include <d3d11.h>
#include <dxgi.h>
#include <xfw_native_hooks_d3d.h>



namespace WoT::Video {
    class D3D {
        //
        // Init
        //
      public:
        D3D(XFW::Native::Hooks::HookmanagerD3D&);
        ~D3D();

        bool Init();
        [[nodiscard]] bool Inited() const;
        void Deinit();


        //
        // Handlers
        //
    public:
        void HandlerDeviceRegister(const std::string& name, std::function<void()> func);
        void HandlerDeviceUnregister(const std::string& name);
        void HandlerPresentRegister(const std::string& name, std::function<void()> func);
        void HandlerPresentUnregister(const std::string& name);
        void HandlerResizeBeforeRegister(const std::string& name, std::function<void()> func);
        void HandlerResizeBeforeUnregister(const std::string& name);
        void HandlerSwapchainRegister(const std::string& name, std::function<void()> func);
        void HandlerSwapchainUnregister(const std::string& name);
    private:
        void handlerDeviceExecute();
        void handlerPresentExecute();
        void handlerResizeBeforeExecute();
        void handlerSwapchainExecute();
    private:
        std::map<std::string, std::function<void()>> m_handler_device;
        std::map<std::string, std::function<void()>> m_handler_present;
        std::map<std::string, std::function<void()>> m_handler_resize_before;
        std::map<std::string, std::function<void()>> m_handler_swapchain;


        //
        // Hooks
        //
      private:
        bool hooksInit();
        void hooksDeinit();

        /// <summary>
        /// Handle IDXGISwapChain::Present() hook
        /// </summary>
        /// <returns>true if swapchain pointer was changed</returns>
        void hooksDxgiSwapchainPresent(IDXGISwapChain *swapchain);

        /// <summary>
        /// Handle IDXGISwapChain::ResizeBuffers() hook
        /// </summary>
        /// <returns>true if swapchain pointer was changed</returns>
        void hooksDxgiSwapchainResizeBuffersBefore();

        /// <summary>
        /// Handle IDXGISwapChain::ResizeBuffers() hook
        /// </summary>
        /// <returns>true if swapchain pointer was changed</returns>
        void hooksDxgiSwapchainResizeBuffersAfter(IDXGISwapChain *swapchain);

    private:
        XFW::Native::Hooks::HookmanagerD3D& m_hooks_mgr;


        //
        // IDXGISwapChain
        //
      public:
        Microsoft::WRL::ComPtr<ID3D11Resource> SwapchainGetBackbuffer();
        Microsoft::WRL::ComPtr<ID3D11Texture2D> SwapchainGetBackbufferCopy(bool cpuAccess);
        std::vector<uint8_t> SwapchainGetBackbufferPixels();
        [[nodiscard]] DXGI_FORMAT SwapchainGetFormat() const;
        [[nodiscard]] uint32_t SwapchainGetHeight() const;
        [[nodiscard]] uint32_t SwapchainGetWidth() const;
      private:
        void swapchainUpdate(IDXGISwapChain* swapchain);
      private:
        IDXGISwapChain *_swapchain = nullptr;
        DXGI_SWAP_CHAIN_DESC m_swapchain_desc{};


        //
        // ID3D11Device
        //
      public:
        ID3D11Device *DeviceGet();
      private:
        void deviceUpdate();
      private:
        ID3D11Device *_device = nullptr;



        //
        // ID3D11DeviceContext
        //
      public:
        ID3D11DeviceContext* DeviceGetContext();
      private:
         ID3D11DeviceContext *_deviceContext = nullptr;



        //
        // ID3D11Resource
        //
      public:
        bool ResourceCopy(ID3D11Resource *destination, ID3D11Resource *source, bool multisampled = false,
                          DXGI_FORMAT format = DXGI_FORMAT_UNKNOWN);
        bool ResourceCopy(Microsoft::WRL::ComPtr<ID3D11Resource> &destination, Microsoft::WRL::ComPtr<ID3D11Resource> &source,
                          bool multisampled = false, DXGI_FORMAT format = DXGI_FORMAT_UNKNOWN);
        bool ResourceCopy(Microsoft::WRL::ComPtr<ID3D11Texture2D> &destination, Microsoft::WRL::ComPtr<ID3D11Texture2D> &source,
                          bool multisampled = false, DXGI_FORMAT format = DXGI_FORMAT_UNKNOWN);


        //
        // ID3D11Texture2D
        //
      public:
        Microsoft::WRL::ComPtr<ID3D11Texture2D> Texture2DCreate(uint32_t width, uint32_t height, DXGI_FORMAT format, UINT bind_flags, bool cpu_read,
                                                                bool cpu_write, void *initial_data = nullptr);
        Microsoft::WRL::ComPtr<ID3D11Texture2D> Texture2DCreate(D3D11_TEXTURE2D_DESC& texture_desc);
        Microsoft::WRL::ComPtr<ID3D11Texture2D> Texture2DCreate(D3D11_TEXTURE2D_DESC& texture_desc, D3D11_SUBRESOURCE_DATA& init_data);



        //
        // ID3D11ShaderResourceView
        //
    public:
        Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> SRVCreate(const Microsoft::WRL::ComPtr<ID3D11Texture2D>& texture);
        Microsoft::WRL::ComPtr<ID3D11ShaderResourceView> SRVCreate(const Microsoft::WRL::ComPtr<ID3D11Texture2D>& texture, DXGI_FORMAT format);
    };
} // namespace WoT::Video
