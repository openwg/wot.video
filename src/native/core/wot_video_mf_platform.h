// SPDX-License-Identifier: MIT
// Copyright (c) 2025 Mikhail Paulyshka

#pragma once

// stdlib
#include <cstdint>
#include <limits>

// Windows
#include <d3d11.h>
#include <mfobjects.h>
#include <wrl.h>

// WoT.Video
#include "wot_video_d3d.h"



//
// Class
//

namespace WoT::Video {
    class VideoPlatform {
        // Ctor
      public:
        VideoPlatform();
        ~VideoPlatform();

        // Init
      public:
        bool Init(std::shared_ptr<D3D> d3d);
        void Deinit();

      private:
        bool m_initialized{};

        // D3D
      private:
        void d3dUpdate();

      private:
        std::shared_ptr<D3D> m_d3d;

        // MF
      public:
        IMFDXGIDeviceManager *MFGetDeviceManager();

      private:
        bool MFDeviceSet(ID3D11Device *device);

      private:
        Microsoft::WRL::ComPtr<IMFDXGIDeviceManager> m_mf_manager;
        UINT m_mf_manager_token{std::numeric_limits<uint32_t>::max()};
    };
} // namespace WoT::Video