// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Mikhail Paulyshka

#pragma once

#include <string_view>

namespace WoT::Video {
    namespace D3DShaders {
        extern const std::string_view PS_Sampler_RGB      ;
        extern const std::string_view PS_Sampler_NV12     ;
        extern const std::string_view PS_Sampler_NV12_Mask;
        extern const std::string_view VS_Quad             ;
    }
}