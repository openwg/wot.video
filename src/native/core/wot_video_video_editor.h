// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Mikhail Paulyshka

#pragma once

// stdlib
#include <cstdint>
#include <limits>
#include <map>
#include <string>

// wot.video
#include "wot_video_mf.h"
#include "wot_video_mf_platform.h"
#include "wot_video_mf_sink.h"
#include "wot_video_mf_source.h"



//
// Class
//

namespace WoT::Video {


    /// <summary>
    /// Structure which holds info about fragment add operation result
    /// </summary>
    /// <note>
    /// *_first means timestamp of frame START (frame timestamp)
    /// *_last means timestamp of frame END    (frame timestamp + frame duration)
    /// </note>
    struct ResultFragmentAdd {
        bool success{};
        int64_t timestamp_in_first = std::numeric_limits<int64_t>::min();
        int64_t timestamp_in_last = std::numeric_limits<int64_t>::min();
        int64_t timestamp_out_first = std::numeric_limits<int64_t>::min();
        int64_t timestamp_out_last = std::numeric_limits<int64_t>::min();
    };

    class VideoEditor {
      public:
        VideoEditor() = default;
        ~VideoEditor();


        // Init
      public:
        bool Init(std::shared_ptr<VideoPlatform> platform);
        bool Deinit();


        // Platform
    private:
        std::shared_ptr<VideoPlatform> m_platform;

        // Input
      public:
        bool InputOpen(const std::wstring &filepath);
        bool InputClose(const std::wstring &filepath);
        [[nodiscard]] bool InputExists(const std::wstring &filepath) const;
        bool InputCloseAll();

      private:
        std::map<std::wstring, VideoSource> _input_source{};


        // Output
      public:
        bool OutputCreate(const std::wstring &filepath_in, const std::wstring &filepath_out);
        bool OutputClose();
      private:
        bool outputSinkCreate(const std::wstring &output, VideoSource &reader);
      private:
        std::unique_ptr<VideoSink> m_output_sink{};
        static constexpr uint32_t m_output_audio_bitrate = 192000;

        // Operations
      public:
        bool OperationAddFragment(const std::wstring &filepath_in, int64_t time_start, int64_t time_end);

      private:
        static ResultFragmentAdd operationAddFragmentVideo(VideoSource &video_source, VideoSink &video_sink, int64_t time_start,
                                                           int64_t time_end);
        static ResultFragmentAdd operationAddFragmentAudio(VideoSource &video_source, VideoSink &video_sink, int64_t time_start,
                                                           int64_t time_end, ResultFragmentAdd video_fragment = {});


        //
        // Negotiation
        //
      private:
        static bool negotiateAudio(VideoSource &source, VideoSink &sink);
        static bool negotiateVideo(VideoSource &source, VideoSink &sink);
    };
} // namespace WoT::Video