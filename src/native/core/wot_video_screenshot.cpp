// SPDX-License-Identifier: MIT
// Copyright (c) 2022 Andrey Andruschyshyn
// Copyright (c) 2022 Mikhail Paulyshka

// stdlib
#include <fstream>
#include <functional>
#include <utility>

// wot.video
#include "wot_video_screenshot.h"
#include "wot_video_stb.h"

using namespace std::placeholders;

namespace WoT::Video {

    CaptureScreenshot::CaptureScreenshot(XFW::Native::Hooks::HookmanagerD3D& hookmanager_d3d) : _hooks_d3d(hookmanager_d3d) {}

    CaptureScreenshot::~CaptureScreenshot() {
        Deinit();
    }

    bool CaptureScreenshot::Init(std::shared_ptr<D3D> d3d) {
        _d3d = std::move(d3d);
        _hooks_d3d.init();
        if (_hooks_d3d.inited()) {
            _hooks_d3d.IDXGISwapChain_Present_register("wot_video_screenshot",  XFW::Native::Hooks::HookPlace::Before,
                                                       std::bind(&CaptureScreenshot::onDxgiSwapchainPresent, this, _1, _2, _3));
        }

        _inited = true;
        return true;
    }

    bool CaptureScreenshot::Deinit() {
        if (_hooks_d3d.inited()) {
            _hooks_d3d.IDXGISwapChain_Present_unregister("wot_video_screenshot", XFW::Native::Hooks::HookPlace::Before);
        }

        _inited = false;
        return true;
    }

    bool CaptureScreenshot::Capture(const std::wstring &filename) {
        _filename = filename;
        _active.test_and_set();
        return true;
    }

    void CaptureScreenshot::process() {
        // check flag
        if (!_active.test()) {
            return;
        }
        _active.clear();

        // get backbuffer data
        auto pixels = _d3d->SwapchainGetBackbufferPixels();
        if (pixels.empty()) {
            return;
        }

        // save PNG
        std::ofstream file(_filename, std::ios::binary);
        if (!file.is_open()) {
            return;
        }
        stbi_write_png_to_func(stbi_write_ofstream, &file, _d3d->SwapchainGetWidth(), _d3d->SwapchainGetHeight(), 4U, pixels.data(),
                               _d3d->SwapchainGetWidth() * 4U);
    }

    void CaptureScreenshot::onDxgiSwapchainPresent(IDXGISwapChain *swapChain, UINT, UINT) {
        process();
    }
} // namespace WoT::Video
